

    Controverse Turing & Wilkes : qui est le premier vrai informaticien ?(1er : Architecture des ordinateurs )
        Qui est Wilkes ?
        Qu’est que la controverse Turing & Wilkes
        Qui a raison selon les historiens ?

    Incendie OVH (1er : client - serveur)
        Que s’est-il passer la nuit de 9 au 10 mars 2021 dans le centre de donnée OVH à Strasbourg ?
        Qu’elles ont été les répercussions en France ?

    le web et son histoire (1er : Partie Réseau) :
        Brève histoire du web | CERN 4
        Qui est Tim Berners-Lee ? Où travaille-t-il ?
        A quel souci s’est-il attaquer dans sa profession ?
        Que fait-il en 1989 et en 1990 ?
        Comment à évoluer le web à partir de 1991 ?
        Pourquoi 1994 est “Year of the Web”?

    L’histoire de L’IA (2 sujets) (1er : K plus proche voisin) :
        L'histoire de l'IA - That's AI 8
        des origines à l’hiver de L’IA (exclus) (1950 - 1970)
        L’hiver de l’IA et la renaissance (1970 - 2024)

    Les Captchas (ter):
        Qu’est-ce qu’un Captcha ?
        Quelle était l’utilité à ma base de cette technologie ?
        Quel est le lien entre Les Captchas et l’intelligence Artificielle

    Ada Lovelace (Ter : Paradigme de programmation)
        Qui est-elle ?
        Qu’elle est son lien avec Charles Babbage ?
        Qu’a-t-elle inventé ? En quoi ces idées sont en avance sur son temps ?
        Qu’est-ce que le Ada Lovelace day ?

    Richard Stallman et les logiciels libres (Ter : Gestion des processus et des ressources par un système d’exploitation.)
        Qui est Richard Stallman ?
        Qu’elle est son problème avec les logiciels propriétaire ?
        Que propose-t-il comme solution ?
        Quelles sont les choses possibles avec un logiciel libres ?

    Panne informatique mondiale de juillet 2024 (Ter : Mise au point des programmes. Gestion des bugs)
        Que s’est-il passer en juillet 2024 avec windows ?
        Qu’elles ont été les conséquences en France ? Dans le monde ?

    Grace Hopper (Ter : Système d’exploitation)
        Qui est Grace Hopper ?
        Quelles sont ses implications dans le domaine informatique ?


        e passage de l’an 2000 : origine du problème, conséquences qui aurait pu être désastreuses
le passage à l’Euro : un autre défi juste après l’an 2000. Le plein emploi pour les informaticiens

l’explosion d’Ariane 5 suite à un souci informatique
