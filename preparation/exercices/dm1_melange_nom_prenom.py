#!/bin/python

# Ne pas oublier de remplacer nom et prénom par votre nom et votre prénom.

def shuffle(L: list) -> None:
    pass


from math import sqrt

def copie_de(L: list) -> list:
    return [x for x in L]

def test_shuffle() -> None:
    L = [i for i in range(1, 4)]
    nb_tests = 10_000
    nb_perm = 6
    p = 1/nb_perm
    listes_melangees = [None] * nb_tests
    for tentative in range(nb_tests):
        shuffle(L)
        listes_melangees[tentative] = copie_de(L)
    permutations  = [[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,2,1], [3,1,2]]
    for melange in permutations:
        freq = listes_melangees.count(melange) / nb_tests
        assert abs(freq - p) <= 1.96 * sqrt(p*(1-p)) / sqrt(nb_tests)

if __name__ == '__main__':
    test_shuffle()
