# Exercices sur les tris

<link rel="stylesheet" href="https://pyscript.net/latest/pyscript.css" />
<script defer src="https://pyscript.net/latest/pyscript.js"></script>

:::{exercise} Trier des valeurs avec la fonction `sorted`
1. À l'aide d'une ou de plusieurs fonctions du module `random`, mélanger une liste `tableau` de 16 valeurs.
2. Appliquer la fonction `sorted` à votre liste `tableau`. Que se passe-t-il ?
3. Lire l'aide de la fonction `sorted` à l'aide de la fonction `help`. En déduire une méthode pour trier dans l'ordre décroissant.
:::

:::{exercise} Trier dans l'ordre décroissant
1.
    <ol type=a>
    <li>Rappeler comment renverser l'ordre d'une liste.</li>
    <li>Quelle est la complexité d'une telle opération ?</li>
    </ol>
2. Sans renverser la liste à la fin
    <ol type=a>
    <li>a. indiquer quelle modification effectuer dans l'algorithme de tri par sélection pour trier dans l'ordre décroissant.</li>
    <li>b. même question que précédemment avec l’algorithme de tri à bulles.</li>
    </ol>
:::

::::{exercise} Changer la fonction de tri
On considère l'algorithme suivant :
```{code-block} python
def tri_bulles(t: list) -> list:
    for i in range(len(t),1,-1):
        for j in range(i - 1):
            if t[j+1] < t[j]:
                t = echanger(t,j,j+1)
    return t
```
1. Écrire `t[j+1] < t[j]` sous la forme d'une fonction dont le prototype est donné :
    :::{code-block} python
    def est_plus_petit(a, b) -> bool:
        pass
    :::
    Cette fonction doit renvoyer `True` si `a` est plus petit que `b`, `False` sinon et échouer si `a` et `b` ne sont pas comparables.
2. À l'aide de la fonction proposée en fin de première activité, vérifier qu'une telle méthode permet de trier des cartes.
::::

<py-repl id="my-repl" auto-generate="true">cartes = [7, 'Dame', 10, 'Roi', 'Valet', 9, 8, 'As'] </py-repl>
