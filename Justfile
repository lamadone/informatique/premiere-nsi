alias venv := create-venv

python_dir := "./.venv/bin"
python := python_dir + "/python3"

cosmetic: build
  find jupyter/_build -regex '*.html' -exec \
  sed -r -i \
    -e s/Exercise/Exercice/g \
    -e "s/Solution to/Solution de l'/g" \
    -e s/Definition/Défintion/g \
    -e s/Theorem/Théorème/g \
    -e s/Example/Exemple/g \
    '{}' \;

build: install
  {{ python_dir }}/jupyter-book build jupyter/

build-jupyter: install
  {{ python_dir }}/jupyter-book build --builder=custom --custom-builder=jupyter jupyter

clean:
  {{ python_dir }}/jupyter-book clean jupyter

create-venv:
  if test -d .venv ; then \
    globali=$(python --version) ; \
    locali=$({{ python_dir}}/python --version) ; \
      if test "${globali}" != "${locali}" ; then \
        rm -r .venv && python -m venv .venv ; \
      fi ; \
  else  \
    python -m venv .venv ; \
  fi

install: create-venv
  if test ! -z ${CI+x} ; then \
    {{ python }} -m pip install -r requirements.txt ; \
    {{ python }} -m bash_kernel.install --user ; \
  fi
  if [ requirements.txt -nt .venv ] ; then \
    {{ python }} -m pip install -r requirements.txt ; \
    {{ python }} -m bash_kernel.install --user ; \
  fi
  touch .venv
# Sur la ligne précédente, on peut ajouter --name=blah

