---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Le protocole HTTP

Le protocole HTTP fait partie de la couche application du modèle TCP/IP.

Le protocole HTTP (HyperText Transfer Protocol) définit plusieurs verbes correspondant à des actions. Parmi ces actions, on dispose de :
- `GET` pour récupérer une ressource
- `POST` pour envoyer une ressource
- `HEAD` pour demander un entête
- `PUT` pour envoyer une ressource sans modification d'état
- `DELETE` pour supprimer une ressource
- `UPDATE` pour mettre à jour une ressource existante.

En classe de Première, on étudiera essentiellement les deux premières.

## Récupération d'une ressource

Pour récupérer une ressource, on utilise le verbe HTTP `GET` suivi de la ressource. Une façon de faire avec Python est d'utiliser la bibliothèque `http` et plus particulièrement le rayon `http.client`.

::::{note}
:class: margin
`http.client` fait partir de la bibliothèque standard, au même titre que `socket` ou `urllib3` que nous ne développerons pas là. Une bibliothèque populaire est `requests` mais il faut l'installer avec
:::{code-block}
pip install requests
:::
::::

:::{note}
:class: margin
Il s'agit de la requête effectué lorsque vous tapez `http://httpbin.org/get` dans la barre d'adresse de Firefox.
:::

:::::{tab-set}
::::{tab-item} Avec `socket`
```{code-block} pycon
>>> s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
>>> s.connect(("httpbin.org",80))
>>> s.send(b"GET /get\nHost: httpbin.org\n\n")
27
>>> r = s.recv(1024)
>>> r
```
::::
::::{tab-item} Avec `http.client`
```{code-block} python
import http.client
conn = http.client.HTTPConnection('httpbin.org:80')
conn.request('GET', '/get')
res = conn.getresponse()
data = res.read()
data
```
::::
::::{tab-item} Avec `request`
```{code-block} python
import requests

response = requests.get('http://httpbin.org/get')
response.text
```
::::
:::::

:::{note}
En réalité, la traduction du nom de domaine (`httpbin.org`) en une adresse IP est une fonction du système d'exploitation. Celle-ci est accessible dans le module `socket`.
:::

Ainsi, la fonction `socket.gethostbyname` renvoie la première adresse IP correspondant au nom passé en argument :
```{code-cell} python
import socket
socket.gethostbyname('httpbin.org')
```
Mais, une même adresse IP peut héberger plusieurs hôtes :
```{code-cell} python
import socket
ip = socket.gethostbyname('httpbin.org')
socket.gethostbyaddr(ip)
```

Les 3 méthodes proposées ici vont de la plus rudimentaire à celle avec le plus haut niveau d'abstraction, mais il est nécessaire de bien comprendre ce qui se passe.

Le verbe `GET`, suivi de la ressource `/` permet de récupérer le document servi par le chemin (on parle de `path_info`) précisé. Ce chemin peut correspondre à un chemin de fichier de type Unix, comme vu dans le [chapitre sur le shell](../5_architecture_materiel_OS/shell.md).

On peut aussi passer des informations dans la ressource avec le `query_string`, c'est-à-dire la partie de l'url située après le caractère `?`.

:::{warning}
Lorsqu'on utilise le protocole HTTP, l'intégralité du message est transmis en clair.
:::
```{code-cell} python
:tags: [remove-input]
from scapy.all import sniff
capture = sniff(offline='get.dump')
capture[5].show()
capture[7].show()
```

## Envoi d'information

L'intérêt de la requête `GET` vue précédemment réside dans le fait que celle-ci peut s'enregistrer pour être effectuée de la même façon et renvoyer un résultat prédictible. Par exemple, l'url `https://www.openstreetmap.org/?mlat=48.89274&mlon=2.36100#map=19/48.89274/2.36100` peut être partagée pour fournir une carte des rues autour de l'établissement.

Cependant, cette méthode ne permet pas d'envoyer des informations à un serveur pour en modifier son état. Pour cela, on utilisera `POST` ou `PUT` [^2].

:::::{tab-set}
::::{tab-item} Avec `socket`
```{code-block} pycon
>>> s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
>>> s.connect(("httpbin.org",80))
>>> s.send(b"POST /post HTTP/1.1\nHost: httpbin.org\nContent-Type: application/x-www-form-urlencoded\nContent-Length: 19\n\nvar1=val1&var2=val2\n")
27
>>> r = s.recv(1024)
>>> r
```
::::
::::{tab-item} Avec `http.client`
```{code-block} python
import http.client
conn = http.client.HTTPConnection("httpbin.org")
conn.request("POST", "/post", "var1=val1&var2=val2")
res = conn.getresponse()
data = res.read()
data
```
::::
::::{tab-item} Avec `request`
```{code-block} python
import requests

response = requests.post('https://httpbin.org/post', data={"var1": "val1", "var2": "val2"})
```
::::
:::::

% https://curlconverter.com/python-httpclient/

% https://fr.wikibooks.org/wiki/Les_r%C3%A9seaux_informatiques/La_couche_application_:_le_web_et_ses_protocoles#Le_protocole_HTTP

[^2]: La différence entre `POST` et `PUT` tient au caractère idempotent, c'est à dire qu'une deuxième requête `PUT` ne fera pas de deuxième commande, alors que deux requêtes `POST` créeront deux commandes.
