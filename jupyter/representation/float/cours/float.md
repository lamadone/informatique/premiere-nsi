---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Représentation machine des nombres décimaux

## Le problème

Effectuer le calcul suivant et indiquer le problème

```{code-cell} ipython
0.2 * 2.3
```

La raison ce cet état de fait tient essentiellement aux puissances décroissantes de 2 et que certains nombres n'ont pas de représentation finie dans cette décomposition [^1]

```{code-cell} ipython
:tags: [remove-input]
from IPython.display import Latex

puissance_2 = "$$\\begin{array}{l}"
for i in range(1,5):
    puissance_2 += f"2^{{-{i}}} = {1/2**i} \\\\"
puissance_2 += "\\end{array}$$"
display(Latex(puissance_2))
```

:::{exercise}
En utilisant les puissances de deux d'exposant négatifs, donner un
encadrement de 0,2.
:::

En fait, l'imprécision est bien trop grande avec cette méthode. La norme
[IEEE 754](https://fr.wikipedia.org/wiki/IEEE_754) permet de représenter les
floatants.

:::{note}
:class: margin

Dans ce document, nous ne faisons pas une présentation complète de la norme,
mais nous l'illustrons sur quelques exemples simples[^2].
:::

:::{kroki}
:type: bytefield
:caption: Représentation d'un floatant en «demi-précision» (16 bits)

(draw-column-headers {:labels (reverse column-labels)})
(draw-box "s")
(draw-box "exposant" {:span 5})
(draw-box "mantisse" {:span 10})
:::

Le bit de poids fort, noté $f$ ici, correspond au signe du nombre. Les 5
nombres suivants (en demi-précision, soit 16 bits) correspond à l'exposant
biaisé. On normalise celui-ci en soustrayant $2^{e-1} - 1$ soit ici 15 car
$e = 5$.
:::{note}
:class: margin
Par exemple, l'exposant biaisé $(11001)_2 = 25$ est normalisé en $25 - 15 =
10$.
:::
Pour finir, la mantisse est la partie décimale d'un nombre, avec la
convention suivante :
  + si l'exposant est 0 ou $2^e - 1$, alors la mantisse $m$ est la partie
    décimale d'un nombre dont la partie entière est 0 (donc un nombre entre
    0 et 1)
  + sinon, alors la mantisse $m$ est la partie décimale d'un nombre dont la
    partie entière est 1 (donc un nombre compris entre 1 et 2)

:::{note}
:class: margin
On a des exceptions :
  + 0 : exposant = 0 et $m = 0$
  + infinis : exposant = $2^e - 1$ et $m = 0$
  + Not a Number (NaN) : $2^e - 1$ et $m ≠ 0$
:::

```{code-cell} python
:tags: [remove-cell]
import struct
def float_to_ieee754(nombre):
  return f"{struct.unpack('>H', struct.pack('!e', nombre))[0]:016b}"

def ieee754_to_float(bits):
  return struct.unpack('!e',struct.pack('>H',int(bits, 2)))[0]
```

On peut voir ça sur quelques exemples, avec des fonctions adhoc.

```{code-cell} python
float_to_ieee754(0.25)
```

```{code-cell} python
ieee754_to_float('0011010000000000')
```

Attention, pour les grands nombres, la différence entre deux nombres
consécutifs peut-être assez grande.

```{code-cell} python
ieee754_to_float('0111101111111111')
```

```{code-cell} python
ieee754_to_float('0111101111111110')
```

Attention, si on représente un nombre avec cette norme là et qu'on revient
en arrière, on ne trouve pas toujours le nombre de départ.

```{code-cell} python
float_to_ieee754(12.1)
```

```{code-cell} python
ieee754_to_float('0100101000001101')
```

:::{warning}
On remarque qu'on ne peut pas représenter tous les nombres.
:::

## Une fonction de comparaison à mettre en œuvre

Pour contourner le caractère discret du calcul des floatants, il est
important de prendre l'habitude de ne pas faire de test d'égalité entre des
floatants, mais de définir une fonction adhoc qui vérifie si les valeurs
sont proches.

:::{note}
:class: margin
On aurait pu utiliser `sys.float_info.epsilon`
:::
```{code-cell} ipython
:tags: [margin]
import sys
sys.float_info.epsilon
```

```{code-cell} ipython
def est_egal(a: float, b: float) -> bool:
    PRECISION = 10**(-10)
    return abs(a - b) <= PRECISION
```

```{code-cell} ipython
est_egal(10,10)
```

```{code-cell} ipython
est_egal(0.2*2.3, 0.46)
```

```{code-cell} ipython
est_egal(0.2*2.3,0.45)
```

[^1]: En fait, c'est plus compliqué que cela
[^2]: En demi précision, on a 16 bits de disponibles soit 1 pour le signe, 5
  pour l'exposant biaisé et 10 pour la mantisse. En simple précision, on 32
  bits, soit 1 pour le signe, 8 pour l'exposant biaisé et 23 pour la
  mantisse. En double précision, on 64 bits, on a 1 bit pour le signe, 11
  pour l'exposant et 52 pour la mantisse.
