---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  name: python3
---

# Sur les codages de caractères

Un ordinateur ne manipule que des nombres binaires, comme 1001, la question qui s'est assez rapidement posée est celle de la représentation des caractères.

```{code-cell} ipython
2**(8 - 1)
```

On rappelle qu'on groupe de 8 bits forme un octet, qui est l'unité de base de la numération dans un ordinateur.

Rappel : un bit est un nombre binaire qui est soit 1 soit 0. C'est la plus petite unité non-divisible, mais ça n'est pas l'unité pratique (qui est l'octet)

Le calcul $2^{8-1} = 128$ correspond au nombre de nombres représentables avec 7 bits. Le dernier bit devait correspondre à un bit de contrôle.

+++

Les chercheurs en informatique et les industriels des années 60 ont établit une correspondance entre les nombres entre 0 et 127 et les caractères de l'alphabet. Cette table de correspondance, élaborée aux États-Unis, s'appelle la **table ASCII**.

+++

La fonction `chr()` permet d'obtenir le caractère associé à un nombre.

```{code-cell} ipython
chr(65)
```

```{code-cell} ipython
chr(95)
```

```{code-cell} ipython
chr(0)
```

Représentons déjà les 8 premiers caractères.

```{code-cell} ipython
def premiers_caractères_ascii():
    r = ""
    for i in range(16):
        r = r + f"{chr(i):2}"
    return r
```

```{code-cell} ipython
premiers_caractères_ascii()
```

Ces 16 premiers caractères sont essentiellement des caractères de contrôle. Il servent à contrôler l'affichage (souvent des impressions dans les années 1960). En particulier, on peut noter :
+ `\n` pour le retour à la ligne
+ `\r` pour le retour chariot (penser à une machine à écrire)
+ `\t` pour la tabulation (4 espaces, bien connue en Python)

```{code-cell} ipython
def table_ascii():
    l = []
    for i in range(8):
        r = ""
        for j in range(15):
            r = r + f"{chr(i*16 + j):3}"
        l = l + [r]
    return l
```

```{code-cell} ipython
table_ascii()
```

La table se complique lorsqu'on veut insérer d'autres caractères.

```{code-cell} ipython
char = "é"
```

```{code-cell} ipython
char.encode("iso8859-1")
```

```{code-cell} ipython
chr(0xe9)
```

```{code-cell} ipython
char.encode('utf-8')
```

```{code-cell} ipython
b'\xe9'.decode('utf-8')
```

```{code-cell} ipython
b'\xc3\xa9'.decode('iso8859-15')
```

La table ASCII ne contenant pas tous les caractères de toutes les langues, celle-ci a été étendue, localement en fonction des besoins d'une langue ou d'une autre. Ainsi, on a eu 
+ iso8859-1 pour le latin étendu (caractères accentués)
+ iso8859-8 pour le turc
+ mac pour les ordinateurs Mac des années 1980/1990

+++

Depuis 1990, le consortium Unicode (qui réunit des industriels et des universitaires) a cherché à unifier les tables de caractères en proposant un système permettant de représenter des millions de graphèmes (unité d'affichage).

Ce sytème est organisé en plans, chacun étant une table de `0x0000` à `0xffff`

```{code-cell} ipython
0xffff
```

Le plan 0 contient à lui tout seul 65535 caractères.

```{code-cell} ipython
chr(0x1f353)
```

Même si un caractère existe dans Unicode, il peut ne pas être représenté si votre système d'exploitation de possède pas de glyphe dans la fonte (police) utilisée.

+++

La fonction python qui permet, pour un caractère donné, d'afficher son point de code Unicode est `ord()`.

```{code-cell} ipython
ord('🍓')
```

```{code-cell} ipython
hex(ord('🍓'))
```

Cette fonction, comme la fonction `chr()` fonctionne caractère par caractère. Si on veut travailler sur une chaine, il faut utiliser les méthodes `.encode()` et `.decode()`.

```{code-cell} ipython
"Une fraise : 🍓".encode()
```

Quelques références 
+ https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(0000-0FFF)
+ https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(1D000-1DFFF)#Symboles_math%C3%A9matiques_alphanum%C3%A9riques
+ https://home.unicode.org/
+ https://unicode-table.com/fr/
+ https://fr.wikipedia.org/wiki/Unicode
