---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Exercices sur la base 16

:::{exercise}
:label: base16

Écrire la fonction d'entête `my_hex(bnr: int) -> str:` dont le comportement doit ressemble à la celui de la fonction native `hex`, permettant de convertir un nombre entier en sa représentation binaire.
:::

:::{begin-solution} base16
:::

```{code-cell}
def my_hex(nbr: int) -> str:
    """
    Convertit un nombre en hexadécimal
    
    >>> my_hex(202)
    '0xca'
    """
    assert type(nbr) == int
    char = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
    resultat = ''
    q = nbr
    while q != 0:
        r, q = q % 16, q // 16
        resultat = char[r] + resultat
    return '0x' + resultat 
```

:::{end-solution}
:::