---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Représentation binaire des nombres des entiers positifs

## Représentation d'un nombre

Un ordinateur ne manipule que des bits, c'est à dire des alternatives qu'on
peut représenter sous la forme 0 ou 1. Un bit est la plus petite quantité
d'information qui peut être transmise à un instant $t$ donné.

:::{prf:definition}
Le regroupement de 8 bits s'appelle un octet.
:::

::::{margin}
:::{note}
En base 10, tout nombre $x$ peut s'écrire $x = d_0 10^0 + d_1 10^1 + … + d_n
10^n$, avec $d_0, … d_n$ des chiffres entre 0 et 9.
En base $b$, tout nombre $x$ peut s'écrire $x = c_0 b^0 + c_1 b^1 + … c_m
b^m$, avec $c_0, c_1, …, c_m$, $m$ symboles correspondant à des nombres
entre $0$ et $b-1$.
:::
::::
Tout nombre entier peut s'écrire en base 2, avec uniquement les symboles $0$
et $1$.

::::{margin}
:::{note}
un mot mémoire est l'unité de base manipulée par le microprocesseur.
:::
::::
Sur un ordinateur, la longueur des mots est conditionnée par la taille des
«mots mémoires» :
- 8 bits : de 0 à 255
- 16 bits : de 0 à 65535

:::{exercise}
:label: taille

Donner le plus grand entier représentable avec
1. 32 bits ;
2. 64 bits ;
3. 128 bits.
:::

:::{solution} taille

```{code-block} python
2**32 - 1
```

```{code-block} python
2**64 - 1
```
:::



## Convention d'écriture en Python en base 2

:::{margin}
:class: note
La fonction `bin()` de Python permet de convertir un entier écrit en décimal
en écriture binaire
La fonction `int(⋅,2)` de Python permet de convertir un entier écrit en
binaire en entier écrit en décimal.
:::
```{code-cell} ipython
:tags: [margin]

int('0b11001010',2)
```

:::{prf:proposition}
Les nombres binaires s'écrivent avec `0` et `1`, en préfixant par `0b`. Il se lisent dans le sens des puissances décroissantes rangées de la gauche vers la droite.
:::

::::{prf:example}
```
0b11001010
  |||||||+- 0×1
  ||||||+-- 1×2
  |||||+--- 0×4
  ||||+---- 1×8
  |||+----- 0×16
  ||+------ 0×32
  |+------- 1×64
  +-------- 1×128
```
Le nombre ainsi représenté est $0×2^0 + 1×2^1 + 0×2^2 + 1×2^3 + 0×2^4 + 0×2^5 + 1×2^6 + 1×2^7 = 202$
::::

Dans la base 10, on dispose de 10 symboles différents, notés 0, 1, 2, …, 9, alors que dans la base 2, on dispose des 2 symboles distincts 0, 1. On peut généraliser à toute base entière $b ≥ 2$, comportant $b$ symboles distincts.
