---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Les nombres en base 16

:::{margin}
La fonction `hex()` de Python permet de convertir un entier écrit en décimal
en écriture hexadécimale.
La fonction `int(⋅,16)` de Python permet de convertir un entier écrit en
hexadécimal en entier écrit en décimal.
:::
```{code-cell} ipython
:tags: [margin]

int('0xca',16)
```

::::::{prf:proposition}
:label: ecriture_hexadecimale

Les nombres hexadécimaux (base 16) s'écrivent avec les symboles `0`, `1`, …,
`A`, …, `F` en préfixant par `0x`. Il se lisent dans le sens des puissances
décroissantes rangées de la gauche vers la droite.
:::::{grid}
::::{grid-item}
:::{list-table}
:header-rows: 1

* - décimal
  - hexadécimal
  - binaire

* - 0
  - 0
  - 0
* - 1
  - 1
  - 1
* - 2
  - 2
  - 10
* - 3
  - 3
  - 11
* - 4
  - 4
  - 100
* - 5
  - 5
  - 101
* - 6
  - 6
  - 110
* - 7
  - 7
  - 111
:::
::::
::::{grid-item}
:::{list-table}
:header-rows: 1

* - décimal
  - hexadécimal
  - binaire

* - 8
  - 8
  - 1000
* - 9
  - 9
  - 1001
* - 10
  - A
  - 1010
* - 11
  - B
  - 1011
* - 12
  - C
  - 1100
* - 13
  - D
  - 1101
* - 14
  - E
  - 1110
* - 15
  - F
  - 1111
:::
::::
:::::
::::::

::::{prf:example}
:label: representation_hexadecimale
```
0xca
  ||
  |+------- 10×1
  +-------- 12×16
```
Le nombre ainsi représenté est $10×16^0 + 12×16 = 202$
::::

[](../exercices/base16)