---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Opérations sur les nombres binaires

## Quelques généralités

On peut effectuer les opérations sur les nombres binaires, comme l'addition, la soustraction, etc.

````{prf:example}
:label: exemple_retenue
```
11     <--- ligne des retenues
 111010
+
 010101
 ------
1001111
```
````



Attention, sous Python, les nombres binaires sont représentés avec des chaines de caractères, on ne peut donc pas simplement les additionner.

:::{exercise-start}
:label: somme_string
:::
```{code-cell} ipython
:tags: [hide-output]
'0b111010' + '0b010101'
```

Indiquer ce qui ne convient pas quand on effectue le calcul précédent sous Python.
:::{exercise-end}
:::

:::{exercise}
:label: serie_somme_entiers
Effectuer les sommes suivantes :
1. 1101 et 0101
2. 1101 + 1001
3. 101101 + 1011
4. 10101 + 11
5. 11011001 + 110110
6. 0010 0110 + 1000 1110
:::



## Les opérations « simples »

Commençons par l'addition

:::{exercise}
:label: addition_entiers_positifs
Effectuer les opérations suivantes.

1. $(010010)_2  + (101011)_2$
1. $(011111)_2  + (110110)_2$
1. $(110110)_2  + (110110)_2$

:::

:::{note}
:class: margin
On peut remarquer que la somme de deux nombres à $n$ bits est un nombre à au plus $n+1$ bits.
:::

:::{warning}
Attention, sur $n$ bits, les bits de rang supérieur ou égal à $n$ sont tronqués, en particulier 
:::

L'additionneur, ou plus précisément le demi-additionneur est très simple à réaliser. En effet, l'addition de deux bits $A$ et $B$ s'obtient avec les `A ^ B` où `^` est l'opérateur « ou exclusif » (`XOR`) et la retenue s'obtient avec un `A & B` où `&` est l'opérateur « et ». Cette notion sera précisée dans le cours [sur les booléens](../../booleens/intro)

## Multiplication

Dans les exemples d'addition, on a vu que la multiplication par 2 (10 en binaire) correspondait à un décalage des bits vers la gauche.

Convertir le nombre 54 en binaire.
```{code-cell} python
:tags: [hide-output]
bin(54)
```

Décaler les bits d'un rang vers la gauche.
```{code-cell} python
:tags: [remove-input,hide-output]
print(f"{54:b}\n{54 << 1:b}")
```

:::{warning}
:class: margin
Python peut représenter les nombres binaires avec les $f$-strings et le _modifier_ `:b`
:::


La multiplication complète est donc la succession d'addition et de décalage (comme la multiplication usuelle). Pour la division, on procède par décalage à droite, en ajoutant des `0` à gauche.


