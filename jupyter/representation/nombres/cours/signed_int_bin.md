---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.9.7 64-bit
  name: python3
---

# Les nombres entiers relatifs

## Rappels sur les opérations

Dans (le cours sur l'addition)[addition_bin], on a pratiqué l'addition bits à bits avec retenue.

Pour la soustraction, deux options s'offrent généralement à nous, soit en travaillant uniquement avec des nombres positifs et donc avoir une précondition sur le deuxième argument de la soustraction, soit en autorisant les nombres négatifs et en additionnant l'opposé.

:::{exercise}
:label: proposition_codage_negatifs
Proposer un codage de l'opposé qui soit compatible avec l'addition.
:::

:::{note} Abus de langage
:class: margin
Par abus de langage, on trouve également l'expression complément à deux (car 1+1 = 2)
:::

## Complément à 1 + 1

On va présenter le complément à un plus un. L'idée est de procéder en deux temps pour trouver l'opposé. On commence par écrire le complément à un, c'est à dire le nombre obtenu en remplaçant tous les 0 par des 1 et réciproquement tous les 1 par des 0.

:::{warning}
Le complément à un de $10001011$ est $01110100$
:::

On ajoute ensuite 1 au résultat :

:::{warning}
La somme $01110100 + 00000001$ vaut $01110101$.
:::

On peut effectuer la somme $10001011 + 01110101$.

::::{prf:example}
:label: exemple_addition_retenue
```
 1111111
 10001011
+
 01110101
---------
10000000
```
::::

:::{exercise}
:label: ecriture_entiers_negatifs
1. Écrire les entiers de 1 à 7 écrits sur 3 bits puis écrire leurs opposés
1. Vérifier que 0 est bien son propre opposé
:::

```{code-cell} python
:tags: [remove-cell]
f"{((1<<8)-1)&-3:0>8b}"
```

On s'aperçoit que $8$ et $-8$ sont tous deux encodés de la même façon. On choisit de conserver $-8$.

La plage de nombres représentables de $-2^{n-1}$ à $2^{n-1} - 1$ contenant $2^n$ entiers (comme pour les positifs

:::{warning}
:class: margin
Attention, pour la moitié des entiers, les représentations coïncident, ce qui a conduit à explosion d'Ariane 5 par débordement d'entier.
:::

représentation sous la forme d'un cercle des 8 entiers
