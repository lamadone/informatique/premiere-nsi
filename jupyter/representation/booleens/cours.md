---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Booléens

## Les opérateurs booléens

Les booléens correspondent aux deux valeurs `True` ou `False`.

On peut considérer les opérations suivantes :

La conjonction :
:::{list-table}
:header-rows: 1
:width: 25%

* - `A`
  - `B`
  - `A AND B`
* - `True`
  - `True`
  - `True`
* - `True`
  - `False`
  - `False`
* - `False`
  - `True`
  - `False`
* - `False`
  - `False`
  - `False`
:::

La disjonction
:::{list-table}
:header-rows: 1
:width: 25%

* - `A`
  - `B`
  - `A OR B`
* - `True`
  - `True`
  - `True`
* - `True`
  - `False`
  - `True`
* - `False`
  - `True`
  - `True`
* - `False`
  - `False`
  - `False`
:::

La négation
:::{list-table}
:header-rows: 1
:width: 25%

* - `A`
  - `NOT A `
* - `True`
  - `False`
* - `False`
  - `True`
:::

## Les opérateurs bits à bits

L'opérateur `&`

```{code-cell} ipython
:tags: [remove-input]
for i in [0, 1]:
    for j in [0, 1]:
        print(f"+---------+")
        print(f"|{i} & {j} → {i & j}",end="|")
        print()
print(f"+---------+")
```

L'opérateur `|`

```{code-cell} ipython
:tags: [remove-input]
for i in [0, 1]:
    for j in [0, 1]:
        print(f"+---------+")
        print(f"|{i} | {j} → {i | j}",end="|")
        print()
print(f"+---------+")
```

L'opérateur `~`

```{code-cell} ipython
:tags: [remove-input]
for i in [0, 1]:
    print(f"+------+")
    print(f"|~{i} → {~i + 2}",end="|")
    print()
print(f"+------+")
```
