# Représentation de la «vérité»

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Valeurs booléennes : 0, 1. Opérateurs booléens : and, or, not.
    Expressions booléennes
  - Dresser la table d’une expression booléenne
  - Le ou exclusif (xor) est évoqué.
    Quelques applications directes
    comme l’addition binaire sont
    présentées.
    L'attention des élèves est attirée
    sur le caractère séquentiel de
    certains opérateurs booléens
```

```{tableofcontents}
```
