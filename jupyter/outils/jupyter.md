# Fonctionnement de Jupyter

Les carnets Jupyter
```{margin}
On parle aussi de notebook
```
sont une façon pratique de mélanger du texte et du code informatique. Ils sont organisés sur le principe de cellules qui contiennent du texte :
- les cellules Markdown (`md`) contiennent du texte interprété suivant un formatage souple
- les cellules Python (`py`) contiennent du code interprété par Python.
```{margin}
Le code Python est éxécuté par votre navigateur, sur votre ordinateur.
```

## Quelques éléments de syntaxe Markdown

Le markdown est un langage de balisage souple permettant de rapidement formater un texte tout en lui conservant un caractère lisible.

Quelques exemples de syntaxe :

### Les titres

::::{grid}
:gutter: 3

:::{grid-item-card} Syntaxe
`# Un titre de premier niveau`

`## Un titre de second niveau`

`### Un titre de troisième niveau`
:::

:::{grid-item-card} Rendu
<h1> Un titre de premier niveau</h1>
<h2> Un titre de deuxième niveau</h2>
<h3> Un titre de troisième niveau</h3>
:::
::::

### Les mises en valeur

::::{grid}
:gutter: 3

:::{grid-item-card} Syntaxe
`**du gras**`

`_de l'italique_`

`<span style="color: red">du rouge</span>`
:::

:::{grid-item-card} Rendu
**du gras**

_de l'italique_

<span style="color: red">du rouge</span>
:::
::::

### Les liens

::::{grid}
:gutter: 3

:::{grid-item-card} Syntaxe
`[^1]` et `[^1]:` en fin de document pour une note interne de bas de page

`[Capytale](https://capytale2.ac-paris.fr)`

`![text alternatif](https://lamadone.forge.apps.education.fr/-/informatique/premiere-nsi-dev/-/jobs/110507/artifacts/public/_static/Breezeicons-devices-64-computer.svg)`
:::

:::{grid-item-card} Rendu
[^1]

[^1]: en fin de document pour une note interne de bas de page

[Capytale](https://capytale2.ac-paris.fr)

![text alternatif](../images/Breezeicons-devices-64-computer.svg)
:::
::::

### Tableaux
:::::{grid}
::::{grid-item-card} Syntaxe

:::{code}

| Colonne 1 | Colonne 2 |
| --- | --- |
| Text | Text |

:::
::::

::::{grid-item-card} Rendu

| Colonne 1 | Colonne 2 |
| --- | --- |
| Text | Text |

::::
:::::

## Exécution du code

Attention, le code du Notebook est exécuté dans votre navigateur. Cette solution permet d'éviter un certain nombre de problématiques.

Cependant, dans le cadre de travail en collaboration (participer avec un camarade, envoyer un projet à son professeur, compléter un T.D.) il est essentiel d'enregistrer son travail ![sauvegarder](image-4.png).
