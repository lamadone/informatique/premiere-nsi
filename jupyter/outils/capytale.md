# Présentation de Capytale

[Capytale](https://capytale2.ac-paris.fr/web/c-auth/pvd/mln/connect) est un service mis à disposition des élèves et des professeurs. Il fournit un environnement sécurisé de programmation permettant de compléter des devoirs et de partager des copies avec son enseignant.

L'utilisation de Capytale se fait de façon identifiée via les **même** identifiants que ceux de l'ordinateur de la Région Île de France.

Une fois connecté, vous pourrez accéder à l'activité dont le code est fourni en cliquant ici
![Accéder à une activité](image.png).

Alternativement, vous pouvez créer vous même votre propre activité
![Créer une activité](image-1.png)

Pour produire du code Python, répondant à un exercice ou à une séance de travail dirigé, le meilleur service fourni par Capytale est le Notebook Jupyter.
![Notebook Jupyter](image-2.png)

Pour produire un site web, l'éditeur suivant vous permettra de produire rapidement
![Site web](image-3.png)
