---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Un type élaboré : le dictionnaire

Lire et effectuer les activités sur [le cours de Mme Coilhac sur les dictionnaires](https://mcoilhac.forge.aeif.fr/dictionnaires/dictionnaires/01_quesako/)

## Définition

:::{margin}
La notion de temps d'accès sera étudiée plus tard dans l'année.
:::

:::{prf:definition}
Un dictionnaire est une collection de couples `(clef, valeur)` unique avec un temps d'accès assez rapide.
:::

:::{margin}
On peut aussi initialiser un dictionnaire avec `dict()`.
:::

:::{note}
Un dictionnaire s'écrit entre `{}` sous la forme `{clef: valeur}`. `{}` représente un dictionnaire vide.
:::

:::{prf:property}
Pour des raisons de performances, la clef doit être immuable.
:::

:::{margin}
Le dictionnaire a été récupéré depuis l'api d'OpenMeteo : https://api.open-meteo.com/v1/forecast?latitude=48.8534&longitude=2.3488&current=temperature_2m,relative_humidity_2m,apparent_temperature,precipitation,cloud_cover,wind_speed_10m,wind_direction_10m,wind_gusts_10m&timezone=auto le jeudi 6 février 2025 à 21h34, heure de Paris.
:::


:::{note}
L'accès à un élément d'un dictionnaire se fait avec la syntaxe indicielle :
:::
```{code-cell}
meteo = {
  "latitude":48.86,
  "longitude":2.3399997,
  "generationtime_ms":0.10538101196289062,
  "utc_offset_seconds":3600,
  "timezone":"Europe/Paris",
  "timezone_abbreviation":"GMT+1",
  "elevation":43.0,
  "current_units":{
    "time":"iso8601",
    "interval":"seconds",
    "temperature_2m":"°C",
    "relative_humidity_2m":"%",
    "apparent_temperature":"°C",
    "precipitation":"mm",
    "cloud_cover":"%",
    "wind_speed_10m":"km/h",
    "wind_direction_10m":"°",
    "wind_gusts_10m":"km/h"
  },
  "current":{
    "time":"2025-02-06T21:30",
    "interval":900,
    "temperature_2m":4.7,
    "relative_humidity_2m":71,
    "apparent_temperature":0.4,
    "precipitation":0.00,
    "cloud_cover":100,
    "wind_speed_10m":14.6,
    "wind_direction_10m":57,
    "wind_gusts_10m":28.8
  }
}

meteo["latitude"]
```


## Utilisation d'un dictionnaire

On a vu précédement comment obtenir une clef, ainsi que la syntaxe d'un dictionnaire.

:::{note}
On peut modifier une clef par un appel direct : `meteo["elevation"] = 37` changera la valeur associée à la clef `"elevation"` pour y affecter la valeur `37`.
:::

:::{prf:property}
Le mot clef `in` permet à la fois de tester l'appartenance d'une clef à un dictionnaire et de parcourir un dictiontionnaire.
:::

```{code-cell}
:class: example

"elevation" in meteo
```

```{code-cell}
:class: example

"timestamp" in meteo
```

```{code-cell}
:class: example

for information in meteo["current"]:
    print(information)
```

:::{exercise} Renvoyer les valeurs
:label: renvoi_valeur
Écrire un script qui renvoie une liste de couples grandeur, valeur, unité avec les données météo.
:::

```{solution-start} renvoi_valeur
:label: ex1
:class: dropdown
```
```{code-cell}
accumulateur = []
for grandeur in meteo["current"]:
    accumulateur = accumulateur + [(grandeur, meteo["current"][grandeur], meteo["current_units"][grandeur])]
accumulateur
```
```{solution-end}
```

:::{prf:property}
La fonction `len` renvoie la longueur d'un dictionnaire.
:::

```{code-cell}
:class: example

len(meteo)
```

## Clefs, valeurs ou éléments.

### Clefs

On peut avoir besoin de la liste des clefs d'un dictionnaire. Pour de petits dictionnaire, on peut procéder de la façon suivante :

```{code-cell}
[clef for clef in meteo]
```

:::{margin}
+ L'objet renvoyé est une collection qu'on peut parcourir.
+ En terminale, nous verrons que cette appel de fonction peut-être remplacé par un appel à la méthode `.keys` avec `meteo.keys()`.
:::

Néanmoins, il existe une fonction des dictionnaires qu'on peut utiliser directement et qui renvoie un objet qu'on peut transformer en `list`.

```{code-cell}
dict.keys(meteo)
```

### Valeurs

Pour obtenir les valeurs d'un dictionnaire, on peut procéder de la façon suivante :

```{code-cell}
[meteo[clef] for clef in meteo]
```

De la même façon, il existe une fonction `values` : 

```{code-cell}
dict.values(meteo)
```

### Items

Pour obtenir les valeurs d'un dictionnaire, on peut procéder de la façon suivante :

```{code-cell}
[(clef, meteo[clef]) for clef in meteo]
```

:::{margin} Parcours par double élément
On peut parcourir par `tuple`.
:::
```{code-cell}
:tags: [margin]

for (clef, valeur) in [('1', 'un'), (2, 'deux')]:
    print(valeur, clef)
```

De la même façon, il existe une fonction `items` : 

```{code-cell}
dict.items(meteo)
```
