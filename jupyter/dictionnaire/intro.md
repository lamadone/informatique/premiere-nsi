# Dictionnaire

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Dictionnaires par clés et
valeurs
  - Construire une entrée de dictionnaire.
    Itérer sur les éléments d’un dictionnaire.
  - Il est possible de présenter les
    données EXIF d’une image sous
    la forme d’un enregistrement.
    En Python, les p-uplets nommés
    sont implémentés par des
    dictionnaires.
    Utiliser les méthodes keys(),
    values () et items ().
```
