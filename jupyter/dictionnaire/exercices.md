---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: 'Python 3.7.6 64-bit (''base'': conda)'
  name: python3
---

# Exercices sur les dictionnaires

::::{exercise}
Un professeur de NSI décide de gérer les résultats de sa classe sous la forme d'un dictionnaire :
+ les clefs sont les noms des élèves ;
+ les valeurs sont des dictionnaires dont les clefs sont les types d'épreuves et les valeurs des tuples `(note, coefficient)`.
:::{code-block} python
resultats = { "Dupont": {
  'DS1': (15.4, 3),
  'DM1': (19, 0.5)
},
"Durant": {
  'DS1': (12.7, 3),
  'DM1': (11, 0.5),
  'PROJET1': (15, 2)
}
}
:::
1. Quelle note a obtenu Dupont au DS1. Indiquer comment obtenir cette valeur.
2. Ajouter une note de projet (coefficient 2) à l'élève Dupont.
3. On souhaite modifier la note de DS de Durant pour corriger en 13.3 coefficient 3, comment faire.
4. Donner une méthode permettant de renvoyer tous les noms d'évaluation de l'élève Durant.
5. Donner une méthode permettant de renvoyer tous les couples `(notes, coefficient)` de Dupont.
6. Que renvoie `resultats.items()` ?
::::


:::{exercise}
En utilisant un dictionnaire, écrire une fonction `hex_to_dec` qui convertit un nombre donné en représentation hexadécimal en sa représentation décimale.
:::

:::{exercise}
Sur les Pokemon : [b0c8-5602876](https://capytale2.ac-paris.fr/web/c/b0c8-5602876)

La première partie se fait avec le cours.

La seconde partie nécessite d'avoir vu la requête `GET`.
:::

```{exercise-start}
:label: ex-mystere-dict
```

```{code-cell} python
table = [ ('Grace', 'Hopper', 'F', 1906),
('Tim', 'Berners-Lee', 'H', 1955),
('Ada', 'Lovelace', 'F', 1815),
('Alan', 'Turing', 'H', 1912) ]
```

Que fait la fonction `fonction_mystere` ?

```{code-cell} python
def fonction_mystere(table):
    mystere = []
    for ligne in table:
        if ligne[2] == 'F':
            mystere.append(ligne[1])
    return mystere

fonction_mystere(table)
```

```{exercise-end}
```
