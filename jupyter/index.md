# Présentation de ce cours

Mon cours d'informatique

## Intentions générales

Le but de ce cours de NSI est essentiellement de servir de support aux activités menées en classe. Il ne remplace pas une prise de notes régulière et personnelle en classe, mais permet de fournir des énoncés de références, aussi bien sur les notions abordées en classe que sur les exercices proposés.


## Table des matières

```{tableofcontents}
```

## Documents officiels

Les textes de référence sont disponibles sur le site [Eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g#sujets-zero).
