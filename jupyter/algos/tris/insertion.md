---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3.8.6 64-bit
  name: python
---

# Tri par insertion

## Quelques outils pour commencer

On commence par se donner une liste de nombres qu'on va mélanger

```{code-cell} ipython
t = [i for i in range(10)]
```

```{code-cell} ipython
t
```

```{code-cell} ipython
from random import shuffle
```

```{code-cell} ipython
shuffle(t)
```

```{code-cell} ipython
t
```

On se donne une fonction qui va échanger la valeur présente à l'indice $i$ avec celle à l'indice $j$.

```{code-cell} ipython
def echanger(t: list, i: int, j: int) -> list:
    assert 0 <= i < len(t), 'Indice i en dehors des bornes'
    assert 0 <= j < len(t), 'Indice j en dehors des bornes'
    t[i],t[j] = t[j],t[i]
    return t
```

On peut désormais vérifier sur quelques exemples.

```{code-cell} ipython
echanger([1,2,3],1,2)
```

```{code-cell} ipython
echanger([0,1],1,1)
```


## Le tri à proprement parler

Une écriture possible de l'algorithme est la suivante :

```{code-cell} ipython
def tri_insertion(t: list) -> list:
    for i in range(len(t)):
        j = i
        while j > 0 and t[j-1] > t[j]:
            t = echanger(t,j-1,j)
            j = j - 1
    return t
```

```{code-cell} ipython
t = [i for i in range(10)]
shuffle(t)
t
```

```{code-cell} ipython
tri_insertion(t)
```

