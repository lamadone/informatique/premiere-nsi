# Les algorithmes de tri

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Tris par insertion, par sélection
  - Écrire un algorithme de tri.
    
    Décrire un invariant de boucle
    qui prouve la correction des tris
    par insertion, par sélection.
  - La terminaison de ces algorithmes
    est à justifier.

    On montre que leur coût est
    quadratique dans le pire cas.

```
