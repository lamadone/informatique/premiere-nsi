---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Activité de découverte

:::{important}
Pour cette activité, vous avez besoin d'une couleur (pique, trèfle, cœur ou carreau) jeu de 32 cartes, ordonnées As, 7, 8, 9, 10, Valet, Dame, Roi.
:::

L'exercice suivant est à réaliser par groupe de 3 :
* un⋅e élève dicte les instructions
* un⋅e élève effectue les instructions
* un⋅e élève note les instructions

:::{exercise}
1. Mélanger les 8 cartes d'une couleur.
2. Disposer les 8 cartes face visible devant vous.
3. Ordonner les cartes suivant l'ordre rappelé ci-dessus en respectant les règles suivantes :
    * vous ne pouvez « sortir » qu'une carte à la fois
    * vous ne pouvez que comparer que deux cartes entre-elles
    * vous ne pouvez déplacer qu'une carte à la fois (on admet qu'on peut échanger deux cartes)
    * vous ne pouvez parcourir les cartes que de la gauche vers la droite (on ne revient pas en arrière)
    * si une tâche est répétitive, vous pouvez la nommer
:::

# Définition

:::{prf:definition} trier
Trier des données dans un tableau (ou une liste Python), c'est les ordonner selon une fonction (booléene) de comparaison à deux entrées.
:::

Dans l'activité proposée, la fonction de comparaison peut s'écrire

```{code-cell} ipython
:tags: [hide-input]

def comparaison_carte(carte_1, carte_2):
    if carte_1 == 'As':
        return True
    if carte_1 == 7 and carte_2 not in ['As']:
        return True
    if carte_1 == 8 and carte_2 not in ['As', 7]:
        return True
    if carte_1 == 9 and carte_2 not in ['As', 7, 8]:
        return True
    if carte_1 == 10 and carte_2 not in ['As', 7, 8, 9]:
        return True
    if carte_1 == 'Valet' and carte_2 not in ['As', 7, 8, 9, 10]:
        return True
    if carte_1 == 'Dame' and carte_2 not in ['As', 7, 8, 9, 10, 'Valet'] :
        return True
    if carte_2 == 'Roi':
        return True
    return False
```

<iframe src="https://animations.interstices.info/methodes-tri/index.html" scrolling="no" width="740" height="410" frameborder="0"></iframe>
