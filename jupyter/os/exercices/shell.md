# Exercices sur le shell

:::{exercise} Escape Game
:label: escage_game_terminus

http://luffah.xyz/bidules/Terminus/
:::

::::{exercise} Représentation des permissions
:label: representation_permission

On considère un fichier avec les permissions suivantes : `-rwxr--r--`.

1. Écrire ces permissions en octal.
2. 
    1. Écrire une fonction Python d'entête `jeton(char: str) -> int:` qui renvoie 0 pour `-`, 4 pour `r`, 2 pour `w`, 1 pour `x` et qui **échoue** dans les autres cas.
    1. Écrire une fonction Python d'entête  `lire_triplet(triplet: str) -> int:` qui renvoie le nombre entre 0 et 7 correspond à la transformation d'un triplet en représentation octale et qui échoue si l'entrée n'est pas un triplet.
    2. Écrire une fonction Python d'entête `lire_permission(permission: str) -> str:` qui renvoie la représentation octale sous forme d'une chaine de caractère d'une permission passée en paramètre et qui échoue si l'entrée ne fait pas 10 caractères.
3. Mettre en œuvre cette fonction dans un fichier Python. On pourra utiliser les fonctions ci-dessous pour tester.
    :::{code-block} python
    def test_jeton():
        assert jeton('-') == 0

    def test_lire_triplet():
        assert lire_triplet('rw-') == 6

    def test_lire_permission():
        assert lire_permission('-rw-rw----) == '0660'
    :::
::::
