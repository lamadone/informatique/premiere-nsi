# Système d'exploitation

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Modèle d’architecture
séquentielle
(von Neumann)
  - Distinguer les rôles et les
caractéristiques des différents
constituants d’une machine.
  Dérouler l’exécution d’une
séquence d’instructions simples
du type langage machine.
  - La présentation se limite aux
concepts généraux.
  On distingue les architectures
monoprocesseur et les
architectures multiprocesseur.
  Des activités débranchées sont
proposées.
  Les circuits combinatoires
réalisent des fonctions
booléennes.
* - Systèmes d’exploitation
  - Identifier les fonctions d’un système d’exploitation.
    Utiliser les commandes de base en ligne de commande.
    Gérer les droits et permissions d’accès aux fichiers.
  - Les différences entre systèmes d’exploitation libres et propriétaires sont évoquées.
    Les élèves utilisent un système d’exploitation libre.
    Il ne s’agit pas d’une étude théorique des systèmes d’exploitation.
```
Note : le point sur la réalisation des fonctions booléennes par des circuits combinatoires a été vue dans le chapitre [](../representation/booleens/intro)
