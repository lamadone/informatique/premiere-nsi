---
jupytext:
  cell_metadata_filter: -all
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Bash
  language: bash
  name: bash
---

# Utilisation du shell

Le shell est l'interface de commande historique, développée vers 1970 par Ken Thomson et Brian Ritchie avec Unix. Cette interface permet de contrôler tout le comportement d'un ordinateur :
- exécuter des programmes
- consulter/éditer des fichiers
- copier/déplacer des fichiers

Bien que la conception de cette interface remonte aux année 1970, elle est toujours utilisée de nos jours, en particulier pour la gestion des serveurs sur Internet ou pour l'automatisation de la construction des logiciels.

On peut noter ces deux citations.

> Write programs that do one thing and do it well. Write programs to work together. Write programs to handle text streams, because that is a universal interface.
_(Doug McIlroy)_


> What I saw in the Xerox PARC technology was the caveman interface, you point and you grunt. A massive winding down, regressing away from language, in order to address the technological nervousness of the user.
_([Attributed to an IBM technician lambasting the Apple Lisa, c. 1979](https://www.catb.org/~esr/writings/taouu/html/pr01.html))_

## Savoir se repérer/déplacer

Avant de pouvoir agir sur des fichiers, il faut savoir où on se trouve.

La commande `pwd` permet de savoir où on est.

:::{margin}
Le résultat de cette commande se nomme classiquement `cwd`, pour current working directory.
:::

```{code-cell}
:tags: [remove-cell]

gcwd=`pwd`
```

```{code-cell}
pwd
```

La commande `ls` permet de lister le contenu d'un répertoire :


```{code-cell}
ls -lh
```

::::{margin}
:::{note}
Les permissions peuvent aussi s'exprimer en notation octale (sur quatre octets) :
* le premier octet est à zéro (on l'ignore dans un modèle simple)
* les 3 octets suivants sont à la somme 
    * 4 pour lecture
    * 2 pour écriture
    * 1 pour exécution

Ainsi, `0764` correspond à `-rwxrw-r--`.
:::
::::
Chaque ligne commence par un groupe de 10 caractères : 1 + 3 + 3 + 3 correspondant
* au type de fichier : `-` pour un fichier normal, `d` pour un répertoire ;
* aux permissions : du propriétaire (premier groupe), du groupe (deuxième groupe) et du reste du monde (other)
    * `r` signifie que le propriétaire, groupe ou le reste peut lire le fichier ;
    * `w` signifie que le propriétaire, groupe ou le reste peut écrire dans le fichier ;
    * `x` signifie que le propriétaire, groupe ou le reste peut exécuter le fichier.

:::{note}
Un répertoire doit être exécutable pour être « traversé ».
:::

:::{prf:example}
:label: exemple_permission
* `-rwx------` est un fichier lisible et inscriptible par l'utilisateur seulement ;
* `drwxr-xr-x` est un dossier lisible par tous et dans lequel l'utilisateur peut écrire
* `-rwxr-xr--` est par exemple un fichier python écrit par le propriétaire, lisible et exécutable par le groupe et lisible uniquement par le reste du monde.
:::

## Utiliser des commandes basiques :

Afficher le contenu d'un fichier :


```{code-cell}
cat fichier.py
```

Recherche d'un `motif` dans un `fichier` :

```{code-cell}
grep hello fichier.py
```

Écraser le contenu d'un fichier :

:::::{margin}
::::{note}
- La partie de gauche des opérateurs `>` et `>>` peut-être une commande.
- Si on veut que la partie de droite soit une commande, il faut utiliser un tube (pipe), avec `|`.
  ```shell
  ls | grep "User"
  ```
  cherche si un répertoire s'appelle `User`.
::::
:::::

```{code-cell}
echo "Texte" > fichier
```

```{code-cell}
cat fichier
```

```{code-cell}
echo nouveau > fichier
```

```{code-cell}
cat fichier
```

Écrire à la fin d'un fichier

```{code-cell}
echo Texte >> fichier
```

```{code-cell}
cat fichier
```

## Manipuler les emplacements de fichier

::::{margin}
:::{note}
On distingue les chemins absolus qui commencent par `/`, des chemins relatifs, qui ne commencent pas par `/`.
:::
::::

```{code-cell}
:tags: [remove-cell]

if [ ! -d /tmp/demo ] ; then mkdir /tmp/demo ; fi
cd /tmp/demo
```

```{code-cell}
mkdir dossier
```

```{code-cell}
pwd
```

```{code-cell}
ls -l
```

Changer le répertoire relativement au `cwd` actuel.

```{code-cell}
cd dossier
```

```{code-cell}
pwd
```

Changer le répertoire pour le répertoire parent du `cwd`.

```{code-cell}
cd ..
```

```{code-cell}
pwd
```

Se déplacer dans le répertoire `/chemin/complet` et ce quelque soit l'actuel `cwd`.

```{code-cell}
:tags: [remove-cell]
cd $gcwd
```

```{code-cell}
pwd
cd /tmp/demo
pwd
```

Créer le `repertoire` relativement au `cwd`.

```{code-cell}
mkdir rerpetoire
ls -lh
```

Créer le `répertoire` dans le sous répertoire `/chemin/vers/`.

```{code-cell}
:tags: [remove-cell]
cd $gcwd
```

```{code-cell}
pwd
mkdir /tmp/demo/autre/
ls -lh
cd /tmp/demo
ls -lh
```

::::{margin}
:::{note}
Les chemins `source` et `destination` peuvent aussi être relatifs ou absolus. 
:::
::::

Copier la `source` dans la `destination`.

```{code-cell}
:tags: [hide]
cd $gcwd
```
```{code-cell}
pwd
ls -lh
cp fichier.py /tmp/demo
```

```{code-cell}
ls -lh fichier.py
```

```{code-cell}
ls -lh /tmp/demo
```

::::{margin}
:::{note}
On peut avoir plusieurs fichiers `sources` et `destination` qui est un répertoire. Dans ce cas, les fichiers sont copiés dans la `destination`.
:::
::::

:::{note}
La commande `diff` permet de voir les différences entre deux fichiers textes.
:::

```{code-cell}
diff -Naur fichier.py /tmp/demo/fichier.py
```

```{code-cell}
echo "    hello()" >> /tmp/demo/fichier.py
```

```{code-cell}
diff -Naur fichier.py /tmp/demo/fichier.py
```

Déplacer ou renommer la `source` en `destination`

::::{margin}
:::{note}
On peut spécifier les fichiers en remplaçant certains caractères par `*`. Ainsi, `M*r` correspondra à tous les fichiers qui commencent par `M` et finissent par `r`.
:::
Attention : `*` correspond à tous les fichiers.
::::

```{code-cell}
mv /tmp/demo/fichier.py /tmp/demo/fichier_new.py
ls -lh /tmp/demo
```

Supprimer définitivement le `fichier`

::::{margin}
:::{note}
`rm -rf dossier` supprime un dossier, même s'il n'est pas vide.
:::
::::

```{code-cell}
rm /tmp/demo/fichier_new.py
ls -lh /tmp/demo
```

```{code-cell}
mv /tmp/demo/dossier /tmp/demo/autre
ls -lh /tmp/demo/
```

```{code-cell}
ls -lh /tmp/demo/autre
```

```{code-cell}
rmdir /tmp/demo/autre
```

```{code-cell}
rmdir /tmp/demo/autre/dossier
ls -lh /tmp/demo/autre/
```

Supprimer définitivement un dossier vide.

+++

```{code-cell}
if [ -d /tmp/demo ] ; then
    rm -rf /tmp/demo
fi
if [ -e fichier ] ; then
    rm -rf fichier
fi
```


[](../exercices/shell)
