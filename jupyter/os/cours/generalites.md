# Généralités

## Définion

Un système d'exploitation est le **logiciel** en charge de la gestion du **matériel** et de l'exécution des tâches des utilisateurs.

:::{margin}
- Le caractrère random (aléatoire) de la RAM tient à sa mise en œuvre physique. Le modèle de mémoire de l'OS est très différent.
- La RAM ne conserve des données que si elle est alimentée en énergie. Ce n'est pas le cas du stockage de masse.
:::

Parmi le matériel, on distingue :
* les composants essentiels
    * processeur
    * RAM (Random Access Memory)
    * stockage de masse (HDD ou SSD)
* les composants d'entrée
    * clavier
    * souris
    * webcam
    * scanner
    * micro
* les composants de sortie
    * écran
    * imprimante
    * haut-parleur
* les composants d'entrée/sortie
    * carte réseau (filaire ou radio)
    * écran tactile

Quelques exemples de systèmes d'exploitation :
* Windows 10
* Android
* iOS
* FreeBSD
* Debian GNU/Linux

## Ordonnancement

:::{margin}
Sur un processeur, à un instant donnée, il ne peut s'éxécuter qu'une et une seule tâche
:::

Le rôle du système d'exploitation est d'ordonner les tâches à exécuter. On peut distinguer deux modes de fonctionnement, par lot où les tâches sont exécutées les unes après les autres et le pseudo multitâche ù les tâches semblent s'éxécuter l'une après l'autre.

![](schema_exec.svg)

Dans le cas du traitement par lot (batch), les tâches sont exécutées l'une après l'autre.

Dans le cas du mutli-tâche, les tâches sont lancées, partiellement exécutées, puis leur état est sauvegardée. Ces actions correspondent aux mini-tâches insérées entre les tâches de couleurs.
