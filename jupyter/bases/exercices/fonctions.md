# Exercices sur les fonctions


::::{exercise}
:label: conversion

:::{code-block} python
def conversion(duree_mn):
    heures = duree_mn // 60
    minutes = duree_mn % 60
    return heures, minutes
:::

1.
    1. Donner les arguments de la fonction `conversion`.
    1. Préciser le type des arguments.
2.
    1. Donner les arguments de retour de la fonction.
    2. Préciser le type des arguments de retour.
3. Que renvoie l'appel `conversion(60)` ?

::::


::::{exercise}
:label: positif

:::{code-block} python
def positif(val):
    return int((-1)**(int(val < 0) - 1))*val
    
valeur=positif(-1)
:::

Que vaut la variable `valeur` après exécution de ce code ?


::::

::::{exercise}
:label: moyenne

:::{code-block} python
def moy(x,y):
    return (x + y) / 2
moyenne = moy(3,5)
:::

1. Préciser le type des valeurs d'entrée et de sortie de la fonction `moy`.
2. Que référence la variable `moyenne` après l’exécution de ces lignes ?

::::

::::{exercise}
:label: carre

:::{code-block} python
def carre(val):
    return val * val
def func(val):
    val = val + 1
    return val
valeur = carre(func(5))
:::

Que référence la variable `valeur` ?

::::

::::{exercise}
:label: operation

:::{code-block} python
def operation_1(a, b, c):
    return a + b * c
def operation_2(a, b, c):
    return a * 2 + b
def operation_3(a, b, c):
    return c * 3 - b
i = 5
j = 1
k = 2
resultat_1 = operation_1(i, j, k) + operation_3(k, i, j)

:::

1. Que vaut `resultat_2 = resultat_1 + operation_2(k, j, i)` ?
1. Que vaut `resultat_2 = resultat_1 + operation_2(k, i, j)` ?

::::

::::{exercise}
:label: nb_arguments

Combien d'arguments ont été renseignés dans l'appel de fonction `somme(12.2,5.4,5.5)` ?

::::


::::{exercise}
:label: help

On souhaite connaître le comportement de l'expression suivante `isinstance('101', str)`

Indiquer la bonne réponse :

- [ ] `help()`
- [ ] `help(isinstance)`
- [ ] `help(isinstance())`
- [ ] `help(isinstance('101', str))`

::::

::::{exercise}
:label: assert

Dans le cadre de la programmation d'un jeu de rôle, on souhaite imposer à l’utilisateur d'un fonction `lancer_dé` de lui passer comme entrée un entier `n` entre 4 et 20.

Cette fonction `lancer_dé` renverra un nombre au hasard entre 1 et `n`.

1. Donner l'interface de la fonction.
1. 
    1. Décrire la précondition en utilisant les comparaisons mathématiques.
    2. Écrire deux instructions utilisant le mot clef `assert` pour imposer le respect de ces conditions.
2. Écrire la documentation de la fonction.

*On ne demande pas ici d'écrire la fonction.*
::::

::::{exercise}
:label: syntaxe

Indiquer si la syntaxe des fonctions suivantes et correcte et si oui, ce qu'elle renvoie sur un appel valide.

1.
    :::{code-block} python
    fonction = def ma_fonction(n):
        return n + 1
    :::
1.
    :::{code-block} python
    ma_fonction(n):
        return n + 1
    :::
1.
    :::{code-block} python
    def ma_fonction(n)
        return n + 1
    :::
1.
    :::{code-block} python
    def ma_fonction(n):
        n + 1
    :::
1.
    :::{code-block} python
    def ma_fonction(n: int):
        return n + 1
    :::
1.
    :::{code-block} python
    def ma_fonction(n -> int):
        return n + 1
    :::
::::

::::{exercise}
:label: fonctions_imbriquees

On définit deux fonctions :

:::{code-block} python
def f(x):
    y = 2*x + 1
    return y

def calcul(x):
    y = x - 1
    return f(y)

:::

Quelle est la valeur renvoyée par l'appel `calcul(5)` ?

::::
