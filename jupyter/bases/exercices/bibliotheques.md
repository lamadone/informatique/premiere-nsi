---
jupytext:
  cell_metadata_json: true
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercices sur l'utilisation de bibliothèques

```{code-cell} python
:tags: [remove-cell]

from myst_nb import glue
import random

glue("aide_random_randrange", random.randrange.__doc__)
```

::::{exercise} Exercice 1
:label: lecture_doc

Dans le cadre de la programmation d'un jeu de rôle, on souhaite imposer à l’utilisateur d'un fonction `lancer_dé` de lui passer comme entrée un entier `n` entre 4 et 20.

Cette fonction `lancer_dé` renverra un nombre au hasard entre 1 et `n`.

1. Quel module faut-il utiliser pour avoir accès à la gestion de l'aléatoire ?
2. On donne la documentation de la fonction `randrange` :

    {glue:text}`aide_random_randrange`

    Comment faut-il l'appeler dans la fonction.
3. Écrire la fonction complète.
::::

::::{exercise}
:label: import_module

1. Quel module faut-il importer pour avoir accès la fonction racine carré ?
2. Écrire une fonction qui calcule l'hypoténuse d'un triangle rectangle connaissant la longueur des deux autres côtés.
::::
