# Exercices sur les erreurs

Pour chacun des exercices suivants, indiquer l'erreur, puis proposer une
correction.

_Indication : pour chaque proposition, on peut l'écrire dans un Notebook et observer le résultat._

::::{exercise}
:label: err_syntaxe

```
::::python
if True
    ...
```
::::

::::{exercise}
:label: err_indentation

```
::::python
def ma_fonction():
...
```
::::

::::{exercise}
:label: err_declaration

```
::::python
a + 1
```
::::

::::{exercise}
:label: err_type

```
::::python
resultat = "2" + 2
```
::::

::::{exercise}
:label: err_type_fonction

```
::::python
nombre = int("abc")
```
::::

::::{exercise}
:label: err_index

```
::::python
ma_liste = [1, 2, 3]
print(ma_liste[5])
```
::::


