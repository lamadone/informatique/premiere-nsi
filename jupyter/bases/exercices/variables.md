# Exercices sur les variables

:::{exercise}
:label: nom

1. Répondre aux questions suivantes
    1. Comment nomme-t-on une variable permettant de stocker des valeurs logiques ?
    2. Si on a tapé `a = 5`, quel est le type de `a` ?
2. Que produit l'appel `7 = b` ?

:::

::::{exercise}
:label: unpacking

On entre les valeurs suivantes sur la console :
:::{code-block} pycon
>>> a,b,c = 4,2,1
:::

Représenter ce qui se passe par un schéma avec des flèches

::::

::::{exercise}
:label: affectation

Écrire le tableau d'affectation des variables dans les cas suivants.

1. 
    :::{code-block} python
    a = 2
    b = 3
    c = a ** b
    d = c % b
    :::
2. 
    :::{code-block} python
    a = 3
    b = 2
    c = (b * a) ** b
    d = (c + b) // a
    :::
3.
    :::{code-block} python
    a = 5
    b = 2
    c=a/b
    :::
4.
    :::{code-block} python
    a = 5
    b = 2
    c=a/b
    :::
5.
    :::{code-block} python
    a = 5
    b = 2
    c=a//b
    :::
6. 
    :::{code-block} python
    a = 5
    b = 2
    c=a%b
    :::
7. 
    :::{code-block} python
    a = 6
    b = 2
    c=a/b =
    :::
::::

::::{exercise}
:label: type

1. Donner le type de la variable `rep` si `rep = x**2 + y** 2 == z**2`
2. S'agit-il d'une instruction ou d'une évaluation ?
3. 
    1. Que vaut `rep` si `x = 5`, `y = 5` et `z = 2` ?
    1. Que vaut `rep` si `x = 6`, `y = 8` et `z = 10` ?

::::

::::{exercise}
:label: type_action

Donner le type et le résultat de l'expression `var1 + var2` si c'est possible. Indiquer si le résultat n'est pas possible.

1.
    :::{code-block} python
    var1=4
    var2=7
    :::
1.
    :::{code-block} python
    var1="4"
    var2=7
    :::
1.
    :::{code-block} python
    var1="4"
    var2="7"
    :::
::::


---

::::{exercise}
:label: contenu_var

1. Que contient la variable `cpte` à la fin de l’exécution de ce script ?

    :::{code-block} python
    cpte = 10
    cpte = cpte + cpte
    cpte = cpte + cpte
    :::
2. Que contient la variable `variable2` à la fin de l’exécution de ce script ?

    :::{code-block} python
    variable1 = 5
    variable2 = 2
    variable1 = variable2
    variable1 = variable2 + variable1
    :::
::::

::::{exercise}
:label: type_resultat

Donner le type de `var` dans les cas suivants :

1. `var=45+50`
2. `var="45<50"`
3. `var=45+50+30.0`
4. `var=45<50`

::::

::::{exercise}
:label: affectation_syntaxe

Quelle est la syntaxe correcte pour affecter la valeur 5 à une variable nommée `age` en Python ?

::::


::::{exercise}
:label: _instruction

Si `x=3` et que l'instruction suivante est exécutée :

:::{code-block} pycon
>>> x = x + 2
:::

Quelle sera la nouvelle valeur de `x` ? 

::::
