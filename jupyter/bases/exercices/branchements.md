# Exercices sur les instructions conditionnelles

## Structure `if ... :`, `elif ... :` et `else:`

::::{exercise}
:label: tictactoe

:::{code-block} python
def tictactoe(a):
    if a < 1 :
        return "TIC"
    elif a > 8.5 :
        return "TAC"
    else:
        return "TOE"
:::

Que renvoie l'appel
1. `tictactoe(0)` ?
1. `tictactoe(8.5)` ?
1. `tictactoe(10)` ?
::::

::::{exercise}
:label: if_imbrique

:::{code-block} python
def mystere(a, b):
    reponse = 1
    if a == 0:
        if b == 0:
            reponse = 0
    return reponse
:::
Que renvoie l'appel `mystere(0, 1)` ?
::::

::::{exercise}
:label: if_imbrique_2

:::{code-block} python
def mystere (n) :
    if n % 3 == 0 or n % 5 == 0 :
        if n % 3 == 0 :
            resultat = 'A'
        else :
            resultat = 'B'
    else :
        if n % 5 == 0 :
            resultat = 'C'
        else :
            resultat = 'D'
    return resultat

:::

Quelle est la valeur de `mystere(10)` ?

::::
::::{exercise}
:label: appel

:::{code-block} python
def mystere(a, b):
    if a < b:
        return a * b
    else:
        return 2 * a
:::
Que renvoie l'appel `mystere(3, 5)` ?
::::

::::{exercise}
:label: test_cascade

On considère la fonction `test_cascade` dont le code est donné ci-dessous.
:::{code-block} python
def test_cascade(num):
    resultat = ""
    if num > 3:
        resultat = "3"
        if num < 5:
            resultat = "5"
            if num == 7:
                resultat = "7"
    return resultat
:::

1. Que renvoie l'appel `test_cascade(7)` ?
2. Expliquer le résultat.

::::

::::{exercise}
:label: test_cascade_bis

On considère la fonction `test_cascade` dont le code est donné ci-dessous.
:::{code-block} python
def test_cascade(num):
    if num > 3:
        return "3"
        if num > 5:
            return "5"
            if num == 7:
                return "7"
:::

1. Que renvoie l'appel `test_cascade(7)` ?
2. Expliquer le résultat.
3. Cette fonction renvoie-t-elle toujours quelque chose ?
::::

## Structure `while ... :`

::::{exercise}
:label: while

:::{code-block} python
n = 8.0
while n > 1.0 :
    n = n / 2
:::

Que renvoie l'exécution de ces lignes ?

::::

::::{exercise}
:label: while_imbrique

:::{code-block} python
x = 4
while x > 0 :
    y = 0
    while y < x :
        y = y + 1
        x = x - 1
:::
Quelles sont les valeurs finales de `x` et `y` ?

::::
::::{exercise}
:label: puissance

:::{code-block} python
:linenos:

def puissance (a,m) :
     p = 1
     n = 0
     while n < m :
          p = p * a # <>
          n = n + 1
     return p

:::

À la ligne 5, on pourrait mettre en commentaire que

- [ ] $p = a^n$
- [ ] $p = a^{n - 1}$
- [ ] $p = a^{n + 1}$
- [ ] $p = a^m$

::::

::::{exercise}
:label: seuil

:::{code-block} python
def seuil(n):
    i=0
    resultat=1
    while resultat < n:
         i=i+1
         resultat=resultat*2
    return i
:::

Quel est le résultat de l'instruction `seuil(65)` ?

::::

::::{exercise}
:label: capital

On considère la fonction suivante :
:::{code-block} python
def capital_double (capital, interet):
    montant = capital
    n = 0
    while montant <= 2 * capital:
        montant = montant + interet
        n = n + 1
    return n
:::

1. Expliquer, sans paraphraser le code, ce que fait cette fonction.
2. Proposer une `docstring`.
3. Renommer la variable `n`.

::::



::::{exercise}
:label: somme

Proposer une fonction pour renvoyer la somme des $n$ premiers inverses des entiers naturels.
::::

::::{exercise}
:label: boucle_for

Un jeu de hasard se joue de façon inconditionnelle en 6 manches. Quel type de boucle peut-on utiliser pour programmer un tel jeu ?
::::
