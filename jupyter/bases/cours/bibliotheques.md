---
jupytext:
  cell_metadata_filter: -all
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Sur les bibliothèques

> Namespaces are one honking great idea -- let's do more of those!

Cette dernière phrase du Zen de Python précise qu'une idée géniale est de regrouper certaines fonctions dans des espaces de noms, qu'on appellera bibliothèque.

## Utilisation d'une bibliothèque

Pour utiliser une bibliothèque et les fonctions associés, il faut l'importer.

```{code-cell} python
import random
```

On dispose ensuite des fonctions exposés par cette bibliothèque.

```{code-cell} python
:tags: [margin]
help(random.random)
```

```{code-cell} python
:tags: [scroll-output]
help(random)
```

:::{margin} Attention
:class: alert

Il est important de préfixer les bibliothèques pour éviter les conflits de nommage qui ne sont pas explicités.
:::
```{code-cell} python
:tags: [margin]
from numpy import sqrt
from math import sqrt
sqrt(-1)
```
```{code-cell} python
:tags: [margin]
from math import sqrt
from numpy import sqrt
sqrt(-1)
```
:::{margin}
Ces deux suites d'instructions ne fournissent pas exactement le même résultat.
:::

On y trouve par exemple la fonction `random.random` dont la documentation  nous précise que'elle permet de tirer un nombre dans l'intervalle $[0, 1[$.

```{code-cell} python
random.random()
```

::::{admonition} Remarque
:::{code-block}
import math as m
:::
permet d'importer la bibliothèque `math` sous l'alias `m` et d'utiliser les fonction sous la forme `m.sqrt`.
::::


## Écrire sa propre bibliothèque

Dans le cadre d'un projet, on veut parfois pouvoir regrouper différentes fonctions dans un fichier pour les utiliser après.

Pour cela, il suffit de créer un fichier `.py`, comme dans l'exemple ci-dessous.

:::{literalinclude} ma_bibliotheque.py
:caption: ma_bibliotheque.py
:::

On peut ensuite importer cette bibliothèque et l'utiliser.

```{code-cell} python
import ma_bibliotheque

ma_bibliotheque.est_pair(3)
```

## Les fonctions de bases et les bibliothèques usuelles

Parmi les fonctions de bases du langage Python, on a les suivantes :

- `max` et `min` qui renvoient le maximum et le minimum de plusieurs éléments ;
- `abs` qui renvoie la valeur absolue ;
- `round` qui renvoie l'arrondi avec la précision demandé ;
- `int`, `float`, `bool`, `str` qui permettent de faire des conversions explicites ;
- `type` qui permet de connaître le type d'un objet ;
- `help` qui affiche l'aide d'une fonction.

D'autres fonctions seront données par la suite.

Python a la particularité de venir avec une collection raisonnable de modules standards permettant d'enrichir le cœur du langage. Parmi ces modules, on a :

- `random` pour la gestion de l'aléatoire ;
- `math` pour les fonctions mathématiques ;
- `turtle` pour le dessin de figure.

[](../exercices/bibliotheques.md)
