---
jupytext:
  cell_metadata_json: true
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Les variables informatiques

## Première approche

En Python, une **variable** est un _identifiant_ (une étiquette). L'action d'associer une variable et un contenu s'appelle l'**affectation**.

On peut représenter les affectations avec des flèches.

```{mermaid}

flowchart LR
    A(a) --> B(1)
    C(b) --> B(1)
    D(c) --> E(2)
```

```{sidebar} Instruction et expression

L'affectation est une instruction qui vient modifier l'état interne de la machine. On parle alors d'instruction.
Le membre de gauche de l'affectation est l'identifiant. Le membre de gauche est une expression.
```

::::{margin}
:class: alert

:::{code-block}
7 = a
:::
n'est pas une instruction valable et provoquera l'erreur
`SyntaxError: cannot assign to literal here. Maybe you meant '==' instead of '='?`.
::::
Par exemple,

```{code-block} python

var = expression
```

est une instruction et `expression` peut-être un calcul, ou le résultat d'une fonction.

Connaître l'état interne des variables est une opération essentielle. Pour cela, on peut compléter le tableau des variables qui va expliciter la valeur prise par chaque variable.

````{admonition} Exemple


On considère la suite d'instruction suivante
```{code}
a = 5
b = 8
a = b + a
b = a*b
```

| E | `a` | `b` |
|---|-----|-----|
| 1 |  5  |     |
| 2 |  5  |  8  |
| 3 | 13  |  8  |
| 4 | 13  | 104 |
````

Un tel tableau permet aussi de comprendre ce qui se passe lorsqu'on cherche à utiliser une variable à laquelle aucune valeur n'a été affecté.

::::{margin}
On peut affecter plusieurs variables simultanément.
:::{code-block}
a, b = 1, 2
:::
qui a pour effet d'affecter `1` à `a` et `2` à `b`.
:danger: il faut qu'il y ait le même nombre d'éléments à gauche et à droite.
::::

````{admonition} Exemple
On considère la suite d'instruction suivante
```{code}
a = 5
b = 8
a = c + a
```

| E | `a` | `b` |
|---|-----|-----|
| 1 |  5  |     |
| 2 |  5  |  8  |

Comme `c` n'existe pas, on obtient alors le message `NameError: name 'c' is not defined`.

````

## Typage des variables

:::{margin}
Cette partie sur la nature des variables Python sera détaillée plus tard.
:::

En informatique, on manipule des données de nature plus ou moins différente. La nature des données (et des variables associés) s'appelle le type. Le tableau ci-dessous donne un aperçu des types élémentaires.

:::{margin}
On peut aussi ajouter les deux valeurs suivantes.
:::
```{code-cell} python
:tags: [margin]
type(True)
```
```{code-cell} python
:tags: [margin]
type(False)
```

```{code-cell} python
:tags: [remove-cell]

from myst_nb import glue
glue("type_int", type(1))
glue("type_float", type(1.5))
glue("type_str", type('a'))
```
:::{margin}
On reviendra plus tard dans le cours sur les `float` et les problèmes qu'on peut rencontrer en les utilisant
:::

:::::{grid}
::::{grid-item-card}
:::{code-block} python
type(1)
:::
{glue:text}`type_int`
::::
::::{grid-item-card}
:::{code-block} python
type(1.5)
:::
{glue:text}`type_float`
::::
::::{grid-item-card}
:::{code-block} python
type('a')
:::
{glue:text}`type_str`
::::
:::::

Au type d'une variable correspond des propriétés et des opérations possibles. Ainsi, il est possible de transformer «simplement» un `int` en `float` pour effectuer le calcul
:::{margin}
La transformation du type d'une données en un autre s'appelle le transtypage. En Python, il peut être implicite ou explicite.
:::
```{code-cell} python
1 + 1.5
```
mais en revanche, il n'est pas possible de d'additionner un `int` et un `str`.
```{code-cell} python
'a' + 1
```

## Évaluation des opérations de comparaison

On a vu précédemment que l'affectation était une **instruction** qu'on pouvait identifier grace à la présence du mot-clef `=`.

On peut désormais considérer les expressions de comparaisons et leurs valeurs.

:::{admonition} Expression de comparaison
Une expression de comparaison est une expression qui est **évaluée** à un des booléens `True`/`False`.
:::

Les exemples ci dessous sont à connaître et à savoir utiliser. On considère ici que `a` est une variable qui identifie la valeur 1 et `b` la valeur 2.

```{code-cell} python
:tags: [remove-cell]
a = 1
b = 2
```

- égalité de deux valeurs
```{code-cell} python
a == 1
```
- négation de l'égalité
```{code-cell}
b != 1
```
- strictement supérieur
```{code-cell}
b > a
```
- supérieur ou égal
```{code-cell}
b >= a
```
- strictement inférieur
`<`
- inférieur ou égal
`<=`

Il existe quelques valeurs spéciales en Python. En particulier `None` qui correspond à la valeur nulle. L'expression de comparaison avec `None` peut alors s'écrire

```{code-cell} python
a is None
```

:::{sidebar}
:class: warning

L'évaluation des booléens est prioritaire sur l'affectation.
:::

## Opérations en Python

:::{margin}
Si l'opération nécessite de changer le type de variable et que c'est possible, Python l'effectuera.
:::

Python permet d'effectuer la plus part des opérations mathématiques courantes. Parmi celles-ci, on a

- `+`, `-`, `*` et `/` pour les opérations usuelles sur les nombres ;
- `//` et `%` pour le quotient et le reste de la division entière ;
- `**` pour l'élévation à la puissance d'un nombre.

L'opérateur `+` sert aussi à assembler ensemble des chaines de caractères (on dit concaténer).

:::{margin} Attention
Les opérateurs travaillent avec le type donné.
:::
```{code-cell}
:tags: [margin]
'1' + '0'
```

```{code-cell}
'a' + 'b'
```

:::{sidebar}
:class: warning

L'évaluation des booléens n'est pas prioritaire sur les opérations uselles.
:::

[](../exercices/variables)
