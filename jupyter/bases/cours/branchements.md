# Instructions de contrôles

## Le branchement conditionnel

Il s'agit de la structure de contrôle la plus essentielle : `if`. Ce mot clef permet d'exécuter le bloc de code indenté dessous si la condition est remplie et sinon de passer à la suite.

::::{admonition} Syntaxe
:::{code-block} python
if <condition> :
    ...
:::
::::

:::::{sidebar}
::::{admonition} Exemple
:::{code-block} python
def est_divisible(n: int, q: int) -> bool:
    reponse = False
    if n % q == 0 :
        reponse = True
    return reponse
:::
::::
:::::

Cette structure peut-être étendue avec d'autres tests qui ne sont exécutés que si le premier échoue.

::::{admonition} Syntaxe
:::{code-block} python
if <condition_1> :
    ...
elif <condition_2>:
    ...
:::
::::

:::::{sidebar}
::::{admonition} Exemple
:::{code-block} python
def est_divisible(n: int, q: int) -> bool:
    if n % q == 0 :
        reponse = True
    elif n % q != 0:
        reponse = False
    return reponse
:::
::::
:::::

::::{admonition} Attention
:class: alert

Les deux exemples suivants ne sont pas équivalents.

:::{code-block} python
if <condition_1>:
    ...
elif <condition_2>:
    ...
:::
:::{code-block} python
if <condition_1>:
    ...
if <condition_2>:
    ...
:::
Le premier exemple teste si la `condition_1` est vraie. Si et seulement si la `condition_1` est fausse, alors la `condition_2` est testée.

Le deuxième exemple teste si la `condition_1` est vraie. Indépendamment du résultat du premier test, on teste si la `condition_2` est vraie.
::::

Enfin, pour générer une action par défaut, on utilise le mot clef `else`.

:::{admonition} Utilisation des commentaires
:class: important

Lorsque le code écrit peut sembler dûr à comprendre ou utilise une subtilité, il peut être intéressant de mettre un commentaire, soit en fin de ligne s'il est court avec `... # <mon commentaire>`, soit dans la docstring de la fonction entre `"""`.
:::

::::{admonition} Syntaxe

:::{code-block} python
if <condition_1> :
    ...
elif <condition_2>:
    ...
else:
    ...
:::
::::

:::::{sidebar}

::::{admonition} Exemple

:::{code-block} python
def est_divisible(n: int, q: int) -> bool:
    if n % q == 0 :
        reponse = True
    else:
        reponse = False
    return reponse
:::
::::
:::::

## Boucle conditionnelle

:::{margin}
Un usage courant de cette boucle est utilisé lors de la programmation événementielle dans les jeux vidéos. Voir la page sur [](../../projet/pyxel/intro)
:::
À partir de la structure de contrôle précédente, on peut imaginer répéter une action tant qu'une condition n'est pas remplie.

C'est le rôle du mot clef `while` qui permet de répéter le bloc d'instruction jusqu'à qu'une condition soit remplie.

::::{admonition} Syntaxe

:::{code-block} python
while <condition_1> :
    ...
:::
::::

::::{admonition} Exemple

:::{code-block} python
def second_carre_divisible(n: int) -> int:
    valeur_courante = n + 1
    while valeur_courante**2 % n != 0:
        valeur_courante = valeur_courante + 1
    return valeur_courante**2
:::
::::

## Boucle bornée

Lorsque la condition d'arrêt d'une boucle conditionnelle dépend d'un compteur, on peut considérer qu'on connaît à le nombre d'itérations (répétitions) de l'action à effectuer. On parle alors de **boucle bornée** et celle-ci s'écrit alors de la façon suivante.

::::{admonition} Syntaxe
:::{code-block} python
for variant in range(nombre_d_iterations):
    ...
:::
::::

Attention, la fonction `range` appelée avec le paramètre $n$ renvoie les entiers de $0$ à $n - 1$.

::::{admonition} Exemple
:::{code-block} python
def somme_entiers(n: int) -> int:
    """
    Renvoie la somme des n premiers entiers naturels

    Args:
        n (int): le dernier entier à additionner
    Returns:
        int: la somme des entiers
    """
    somme = 0
    for entier in range(n + 1):
        somme = somme + entier
    return somme
:::
::::

Pour certains types de variables comme les chaînes de caractères, on peut parcourir directement les éléments de la variable.

::::{admonition} Exemple
:::{code-block} python
def compte_lettre(chaine: str) -> int:
    """
    Renvoie le nombre de lettres dans la chaine de caractère

    Args:
        chaine (str): une chaine de caractère
    Returns:
        int: le nombre de caractère
    """
    longueur = 0
    for caractere in chaine:
        longueur = longueur + encaracteretier
    return caractere
:::
::::

[](../exercices/branchements.md)
