---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Écrire des fonctions

## Définition d'une fonction

Les fonctions sont des objets importants dans la conception de l'informatique.

```{admonition} Les fonctions informatiques
:class: warning
Les fonctions en informatique ressemblent beaucoup aux fonctions mathématiques.
```

Une fonction python se définit de la façon suivante :

```{code-block} python
def nom_de_la_fonction(arg1, arg2, ...):
    ...
```

```{sidebar} Mot clef
`def` ici est un mot clef du langage Python. Il ne prend pas de parenthèses mais utilise `:` pour indiquer la fin de ses arguments.
```

Une fonction permet de remplacer un bloc d'instruction répétitif qui dépend de quelques paramètres (voir d'aucun). Pour bien délimiter le bloc d'instruction remplacé, celui ci doit impérativement être indenté, c'est à dire décalé de 2 ou 4 espaces vers la droite.

```{sidebar} Indentation en Python
L'indentation (le décalage d'un bloc vers la droite) est un élément clef de la syntaxe Python. C'est ce qui permet de visualiser les blocs.
```

::::{admonition} À savoir
:class: important
En Python, une fonction renvoie la valeur spécifiée par le mot clef `return`. Si celui-ci n'est pas présent dans le corps de la fonction, celle-ci renvoie la valeur spéciale `None`.

De plus, une fonction s'interrompt au premier `return` rencontré. Par exemple, la fonction
:::{code-block} python
def addition(a, b):
    return 1
    resultat = a + b
    return resultat
:::
renverra toujours la valeur `1`.
::::


Une fonction peut agir directement sur l'état interne. On parle de fonction à effet de bord. C'est le cas de la fonction `print` qui renvoie `None`.

## Prototypage

Lors de la phase de conception d'un programme, on peut donner juste quelques informations sur la fonction, en particulier, on préciser quels sont les arguments en entrée et quelle est la nature de la sortie.

::::{admonition} Exemple
:::{code-block} python
def racine(x: float, n: int) -> float:
    ...
:::
indique que la fonction `racine` prend comme arguments deux nombres `x`, un `float` et `n` un entier et renvoie un nombre de type `float`.
::::

Cette indication de type peut-être complétée par des indications de type dans la documentation de la fonction.

```{code-cell} python
:tags: [remove-cell]
def racine(x: float, n: int) -> float:
    """Renvoie la racine n-ième de x, c'est à dire le nombre y tel que y^n = x

    Args:
        x (float): le nombre dont on veut calculer la racine n-ième
        n (int): l'exposant de la racine
    Returns:
        float: un nombre y tel que y^n = x
    """
    ...
```

::::{admonition} Exemple
:::{code-block} python
def racine(x: float, n: int) -> float:
    """Renvoie la racine n-ième de x, c'est à dire le nombre y tel que y^n = x

    Args:
        x (float): le nombre dont on veut calculer la racine n-ième
        n (int): l'exposant de la racine
    Returns:
        float: un nombre y tel que y^n = x
    """
    ...
:::
complète les informations sur la fonction `racine`.
::::
:::{admonition} `docstring`
:class: important
Le texte entre `"""` s'appelle la `docstring`.
:::
:::{sidebar}
La fonction `help` renvoie précisément le contenu de la `docstring`.
:::
```{code-cell} python
:tags: [sidebar]
help(racine)
```

::::{admonition} Exemple d'utilisation
:class: dropdown

On peut donner un exemple d'utilisation de la fonction.
:::{code-block} python
def racine(x: float, n: int) -> float:
    """Renvoie la racine n-ième de x, c'est à dire le nombre y tel que y^n = x

    Args:
        x (float): le nombre dont on veut calculer la racine n-ième
        n (int): l'exposant de la racine
    Returns:
        float: un nombre y tel que y^n = x
    Examples:
    >>> racine(4, 2)
    2.0
    """
    ...
:::
Les exemples des différentes fonctions peuvent-être ensuite vérifiés pour leur cohérence par
:::{code-block}
from doctest import testmod
testmod(verbose=True)
:::
::::

## Conditions sur les fonctions

Le prototypage présenté dans la partie ci-dessus ne donne que des indications de types sans plus d'informations sur les contraintes qui pourraient exister.

Ainsi dans l'exemple de la fonction précédente, les nombres $x$ et $n$ doivent être strictement positifs pour éviter les cas particuliers.

:::{admonition} Définition

- On appelle **précondition** toute condition portant sur les valeurs de l'entrée d'une fonction.
- On appelle **postcondition** tout condition portant sur la valeur de sortie d'une fonction.

:::

:::{margin}
La syntaxe `assert <condition>, <message>` permet de mettre un message personnalisé.
:::

:::{sidebar}
Une précondition peut se valider avec le mot clef `assert` suivi d'une proposition conditionnelle (par exemple `==`, `!=`, `>`, …)

Si l'évaluation de la proposition renvoie `False`, alors `assert` interrompt l'exécution dela fonction.
:::


::::{admonition} Exemple
:::{code-block} python
def racine(x: float, n: int) -> float:
    """Renvoie la racine n-ième de x, c'est à dire le nombre y tel que y^n = x

    Args:
        x (float): le nombre positif dont on veut calculer la racine n-ième
        n (int): l'exposant positif de la racine
    Returns:
        float: un nombre y positif tel que y^n = x
    """
    assert x > 0
    assert n > 0
    ...
:::
complète les informations sur la fonction `racine`.
::::

## Tester le résultat d'une fonction

Bien que nous n'avons toujours pas détaillé le code de la fonction précédente, nous pouvons étendre la notion de postcondition sur le résultat de la fonction à celui d'un jeu de test.

Pour cela, on peut écrire une nouvelle fonction, préfixée `test_` et postfixée par une information sur la nature du test dont le rôle est de vérifier sur quelques cas particuliers que la fonction possède le comportement attendu.

:::{admonition} Remarque
:class: warning,sidebar
Un jeu de test, même très complet, ne constitue pas une preuve que la fonction est correcte.
:::

::::{admonition} Exemple
:::{code-block} python
def test_racine_entier():
    assert racine(4, 2) == 2
    assert racine(27, 3) == 3
:::
::::
L'exécution de cette fonction `test_racine_entier` sera interrompue si et seulement si un des tests échoue. Ainsi, la rédaction d'un bon choix de fonction test est un préalable à la rédaction d'une fonction.

[](../exercices/fonctions)
