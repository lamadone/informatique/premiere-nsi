# Un exemple de bibliothèque python

# (c) Vincent-Xavier Jumel 2024
def est_pair(n: int) -> bool:
    """Indique si le nombre est pair ou non

    Parameters
    ----------
    n : int
        L'entier à tester

    Returns
    -------
    bool
        True si l'entier est pair
    """
    return bool(1 - n % 2)
