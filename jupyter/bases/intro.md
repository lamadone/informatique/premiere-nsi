---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.2
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Les bases pour bien débuter

```{margin}
La syntaxe regroupe le vocabulaire (les mots de Python) et la grammaire (la façon de les agencer pour produire du sens).
Ainsi, si un mot ou l'agencement de plusieurs mots n'est pas reconnu, Python renverra un message `SyntaxError`.
```

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Constructions élémentaires
  - Mettre en évidence un corpus de constructions élémentaires.
  - affectation, conditionnelles, boucles bornées, boucles non
    bornées, appels de fonction.
* - Spécification
  - Prototyper une fonction.
    
    Décrire les préconditions sur les arguments.
    
    Décrire des postconditions sur les résultats.
  - Des assertions peuvent être utilisées pour garantir des préconditions ou 
    des postconditions.
* - Mise au point de programmes
  - Utiliser des jeux de tests.
  - L’importance de la qualité et du nombre des tests est mise en évidence. Le
    succès d’un jeu de tests ne garantit pas la correction d’un programme.
* - Utilisation de bibliothèques
  - Utiliser la documentation d’une bibliothèque.
  - Aucune connaissance exhaustive d’une bibliothèque particulière n’est
    exigible.
```

Ce cours est l'occasion de brosser un tableau général de concepts généraux et d'éléments de la syntaxe Python. Pour finir ici, quelques éléments de la philosophie des développeurs Python.

```{code-cell} python
:tags: [remove-input]
import this
```


```{tableofcontents}
```
