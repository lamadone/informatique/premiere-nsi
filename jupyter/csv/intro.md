# Données en tables

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires

* - Indexation de tables
  - Importer une table depuis un
fichier texte tabulé ou un fichier
CSV.
  - Est utilisé un tableau doublement
indexé ou un tableau de p-uplets
qui partagent les mêmes
descripteurs
* - Recherche dans une
table
  - Rechercher les lignes d’une
table vérifiant des critères
exprimés en logique
propositionnelle.
  - La recherche de doublons, les
tests de cohérence d’une table
sont présentés.
* - Tri d’une table
  - Une fonction de tri intégrée au
système ou à une bibliothèque
peut être utilisée.
* - Fusion de tables
  - Construire une nouvelle table
en combinant les données de
deux tables.
  - La notion de domaine de valeurs
est mise en évidence.
```
