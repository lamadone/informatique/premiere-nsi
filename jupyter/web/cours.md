---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.6
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Écrire une page web

## Histoire du web

Le web a été inventé au CERN par Tim Berner Lee dans la première moitié des
années 1990, comme outil d’échange pour les scientifiques. L’idée générale
est de permettre aux auteurs de produire facilement des documents structurés
contenant des références (**hyperliens**) vers d’autres documents.

Le web est essentiellement composé de deux entités :
1. un protocole HTTP
2. un langage de description

## Présentation

HTML signifie **H**yper**T**ext **M**arkup **L**anguage. Il s’agit donc d’un
langage de balise (markup) permettant essentiellement d’aller au delà
(Hyper) du texte.

Il descend essentiellement d’autres langages comme SGML et possède une
parenté avec XML qui peut être vu comme un cousin éloigné.

La version courante, HTML5, a été normalisée par le [WordWideWeb Consortium
(W3C)](https://w3.org) depuis plus de 15 ans.

## Principe de fonctionnement

Un des principes posés par le HTML est de séparé le fond (le contenu et la
structure) de la forme (la façon dont les éléments sont affichés. Pour
l’instant, nous nous concentrerons sur le fond.

HTML est un langage à balise et celles-ci sont des éléments structurants
d’une page web, comme les titres, les éléments à mettre en valeur, …

De façon générale, à une balise ouvrante `<tag>`, on associe une balise
fermante `</tag>`. Dans quelques cas, on a des balises auto fermantes :
`<tag/>`.

Ces balises permettent d’ajouter des informations d’ordre sémantique, c’est
à dire d’apporter du sens à la structure du document. Ces balises permettent
de déployer un modèle de document hiéarchique. Ainsi, une page HTML commence
par la balise `<html>` et finit par `</html>`.

Un attribut d’une balise est une information complémentaire écrite dans la
balise qui vient modifier le comportement. Par exemple, l’attribut `href` de
la balise `<a>` permet d’indiquer la cible du lien.

Une page web comporte des métadonnées, dans un bloc de `<head>`. Parmi celles-ci, on retrouve
+ le title `<title>` ;
+ la balise auto fermante `<meta>` qui contiendra des attributs comme la
langue du document.

## Quelques balises utiles

**strong** indique que le texte doit être mis en valeur, alors que *bold*
indique, par exemple, que la mise en valeur doit être en gras (alors que la
mise en valeur pourrait être en rouge). Ces éléments seront précisés
dans un autre cours.

-   `<p>` pour le début d’un paragraphe, `</p>` pour la fin d’un paragraphe ;
-   `<h1>` à `<h6>` pour les titres de niveau 1 à 6 ;
-   `<img src='...' title='...' alt='...' />` pour les images ;
-   `<a href='...'>...</a>` pour les liens ;
-   `<ul><li>...</li></ul>` pour les listes non numérotées et `<ol><li>...</li></ol>` pour les listes numérotées ;
-   `<strong>` pour du texte mis en valeur ;
    :::{margin}
    Dans un texte préformaté, les espaces sont bien rendus, mais pas les \< par
    exemple. Il faudra les échapper avec `&lt;` par exemple.
    :::
-   `<pre></pre>` pour du texte préformaté.

Le site [Mozilla Developer Network](https://developper.mozilla.org), propose
une documentation complète, traduite en français des différentes balises.
Chaque utilisation est complétée par un exemple que vous pouvez modifier à
la volée pour voir le fonctionnement. On peut compléter avec
[w3school](https://w3schools.com/) qui possède également une liste
exhaustive des balises et de leurs usages avec des exemples. Sur

## Exercices

Écrire une page web (`cours.html`) permettant de résumer le premier chapitre
de l’année.

Votre fichier doit être envoyé au site http://validator.w3.org/ afin de
vérifier qu’il ne comporte pas d’erreur grave.

## HTML5

```` {margin} ```{note} Markdown est un langage de balise légère conçu pour être facile à lire et à écrire pour des êtres humains, mais permettant une transformation vers HTML. ``` ````

Aujourd’hui, on n’écrit pas de HTML directement, mais on utilise des générateurs de HTML. Python possède un tel générateur appartenant à la famille Markdown.
