# Les séquences

```{list-table}
:header-rows: 1

* - Contenus
  - Capacités attendues
  - Commentaires
* - Tableau indexé, tableau
donné en
compréhension
  - Lire et modifier les éléments
d’un tableau grâce à leurs
index.
Construire un tableau par
compréhension.
Utiliser des tableaux de
tableaux pour représenter des
matrices : notation a [i] [j].
Itérer sur les éléments d’un
tableau.
  - Seuls les tableaux dont les
éléments sont du même type sont
présentés.
Aucune connaissance des
tranches (slices) n’est exigible.
L’aspect dynamique des tableaux
de Python n’est pas évoqué.
Python identifie listes et tableaux.
Il n’est pas fait référence aux
tableaux de la bibliothèque
NumPy.
* - p-uplets.
p-uplets nommés
  - Écrire une fonction renvoyant
un p-uplet de valeurs.
  -
```

```{tableofcontents}
```
