---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Les chaines de caractères

:::{prf:definition}
:label: str
Une chaine de caractères est un objet Python qui ne contient que des caractères.
:::

