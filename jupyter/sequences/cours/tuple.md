---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Les tuples

:::{prf:definition}
:label: tuple
Un tuple Python est un tableau de valeur qu'on ne peut pas modifier  étendre. Il contient des données de type varié.
:::

```{code-cell} ipython
mon_tuple = (1, 2, 3, 'zero')
tuple_vide = tuple()
```

```{code-cell} ipython
type(mon_liste)
```

```{code-cell} ipython
type(tuple_vide)
```
