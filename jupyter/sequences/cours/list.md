---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.1
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Les listes

:::{prf:definition}
:label: list
Une liste Python est un tableau de valeur qu'on peut modifier et étendre. Il contient des données de type varié.
:::

```{code-cell} ipython
ma_liste = [1, 2, 3, 'zero']
liste_vide = []
```

```{code-cell} ipython
type(ma_liste)
```

```{code-cell} ipython
type(liste_vide)
```

## Listes en compréhension

Il est souvent nécessaire de produire des listes en utilisant une
**expression**
:::{margin}
Une expression est une suite de mot d’un langage pouvant être évaluée, c’est
à dire renvoyer une valeur.
:::

Ainsi, on peut considérer les listes définies en compréhension suivantes.

```{code-cell} ipython
[i for i in range(10)]
```

```{code-cell} ipython
[chr(c + ord('A')) for c in range(26)]
```

Ces compréhensions peuvent être utilisés comme valeur de retour d’une
fonction.

```{code-cell} ipython
:tags: [remove-cell]

dec_bin = bin
verifie_syntaxe = (lambda y: all(map(lambda x: 0<= x < 256, y)))
```

```{code-cell} ipython
IP = [192,168,0,24]

def IP_bin(IP: list) -> list:
    assert len(IP) == 4, f"{IP} ne contient pas 4 octets"
    assert verifie_syntaxe(IP), f"{IP} contient au moins un nombre qui n’est pas sur un octet"
    return [dec_bin(octet) for octet in IP]

IP_bin(IP)
```

## Modification d'une liste

On a vu qu'il est possible de modifier le contenu d'une liste. Par exemple,
l'instruction
:::{margin}
Une instruction est une expression qui va modifier l'état de la machine
:::

```{code-cell} ipython
x = [1, 2, 3, 4]
x[1] = -1
x
```

va créer une liste `x`, puis modifier le contenu de l’élément d’indice 1 de
la liste.

Dans le cas où l’on souhaite ajouter des éléments en fin de liste, on peut
utiliser la méthode `.append`
::::{margin}
En fait, pour une liste `liste`, les instructions `liste.append(valeur)` et
`list.append(liste, valeur)` sont équivalentes.
::::

```{code-cell} ipython
x.append(5)
x
```

Dans le cas où l’on souhaite retirer des éléments en fin de liste, on peut
utiliser la méthode `.pop`
::::{margin}
En fait, pour une liste `liste`, les instructions `liste.pop()` et
`list.pop(liste)` sont équivalentes.
::::

```{code-cell} ipython
x.pop()
x
```

## Accès à une sous-liste

On rappelle que la fonction `range()` peut prendre 1, 2, ou 3 arguments.

1. `range(n)` permet de parcourir les entiers de $0$ à $n-1$.
2. `range(n, m)` permet de parcourir les entiers de $n$ à $m-1$.
3. `range(n, m, p)` permet de parcourir les entiers $n$, $n + 2p$, $n + 3p$
   et ce tant que $n + kp <m $

```{code-cell} ipython
chiffres = [i for i in range(10)]
```

```{code-cell} ipython
chiffres[3]
```

```{code-cell} ipython
chiffres[3:7]
```

```{code-cell} ipython
chiffres[3:7:2]
```

```{code-cell} ipython
chiffres[3:]
```

```{code-cell} ipython
chiffres[:3]
```

```{code-cell} ipython
chiffres[7:3:-1]
```

```{code-cell} ipython
chiffres[::-1]
```
