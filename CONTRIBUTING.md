# Contribuer à ce projet

Ce projet de développement de cours écrit à l'aide de Jupyter Notebooks dans le cadre du cours de Première NSI est aussi l'occasion d'explorer et de démontrer en action le fonctionnement du logiciel Gitlab qui propulse cette forge des communs numériques de l'Éducation Nationale.

Ce projet est divisé en deux parties.
* une partie publique : https://forge.apps.education.fr/lamadone/informatique/premiere-nsi
* une partie privée pour le développement : https://forge.apps.education.fr/lamadone/informatique/premiere-nsi-dev

## Présentation des modes de contributions

### Choix du mode de fonctionnement

La partie publique est configurée pour ne pas permettre les contributions de code directement dessus, sauf en passant par la création d'une branche pour les modifications rapides. Dans ce cas, le système de publication ne permet pas de relecture du site généré avant publication. Cette méthode est à éviter.

### Mode de fonctionnement privilégié

Le mode de fonctionnement privilégié est de travailler dans le dépôt de développement https://forge.apps.education.fr/lamadone/informatique/premiere-nsi-dev en travaillant dans une branche dédiée à la partie en cours de rédaction ou d'amélioration.

### Contribuer de façon ponctuelle avec des tickets

Pour les contributions ponctuelles, vous pouvez envoyer un courriel à l'adresse forge-apps+guichet+lamadone-informatique-premiere-nsi-dev-1268-issue-@phm.education.gouv.fr Cet envoi permettra de créer un [ticket de suivi](https://forge.apps.education.fr/lamadone/informatique/premiere-nsi-dev/-/issues)

Si vous êtes enseignant⋅e de l'Éducation Nationale (public, privé sous contrat, AEFE), vous pouvez créer [un ticket](https://forge.apps.education.fr/lamadone/informatique/premiere-nsi-dev/-/issues/new)

### Contribuer de façon plus régulière

Si vous avez de nombreuses modifications à proposer de façon ponctuelle et que vous êtes familier avec une forge logicielle, le plus simple est de créer votre bifurquation du projet et de proposer une requête de fusion.

Si vous pensez participer de façon régulière, vous pouvez demander l'accès au projet.

## Choix rédactionnels

Cette partie détaille les choix abordés par ce cours.

### Choix techniques

Les fichiers sources au format Markdown (.md) et Jupyter (.ipynb) sont transformées en page web avec le logiciel [Jupyter-book](https://jupyterbook.org/)

### Choix de progressions

Les choix de progressions sont le fruit de l'expérience des auteurs et sont organisés sous la forme de jalons : https://forge.apps.education.fr/groups/lamadone/informatique/-/milestones et ne sont pas discutables pour l'année en cours.

### Choix de style

Ce cours ne se veut pas une vision complète du cours d'informatique, mais un support pour l'enseignant⋅e et ses élèves pour le travail réalisé en classe. Le cours ne vise donc pas à se substituer à un manuel. Pour cela, vous pouvez consulter par exemple https://cours-nsi.forge.apps.education.fr/premiere/

Les activités ne prétendent pas nécessairement à l'originalité et sont souvent le fruit de reprise de discussions sur [le forum MOOC NSI](https://mooc-forums.inria.fr/moocnsi/). Cependant, les activités ou les points de cours réutilisés depuis un livre de cours devraient être référéncées dans [jupyter/references.bib](jupyter/references.bib) sous la forme de bibliographies LaTeX.

Seules des informations essentielles doivent figurer en centre de page. Les informations complémentaires, en particulier non exigible peuvent figurer sous la forme de remarques. Elles apparaitront en colonne de droite.
