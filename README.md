# Présentation du dépot

Ce dépot contient des cours et documents élaborés dans le cadre de
l'enseignement de [spécialité Numérique et Sciences
Informatiques](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
de la classe de [Première](https://eduscol.education.fr/92/j-enseigne-au-lycee-generaltechnologique) pendant les années 2019/2020 (première année de l'enseignement) et 2020/2021.

## Organisation du dépôt

Le dépôt est organisé en plusieurs parties distinctes :
+ [LaTeX](./LaTeX) contient des sources de documments au format $\LaTeX$.
+ [@src](./@src) contient des scripts [Python](https://python.org) pour
générer et distribuer les documents PDF sur plusieurs plateformes.
+ [jupyter](./jupyter) contient des fichiers sources au format .ipynb pour
générer un site de cours.
+ [.gitlab-ci.yml](./.gitlab-ci.yml) contient les informations nécessaires à
la génération des fichiers en utilisant les fonctionnalités d'[intégration
continue/déploiement
continu](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/)
+ [requirements.txt] contient une liste de modules Python nécessaires.

## Méthodologie de contribution

Ce dépôt public [Première
NSI](https://forge.apps.education.fr/lamadone/informatique/premiere-nsi) est
le dépôt public associé à un dépôt interne à la forge, clone du précédent :
[Première NSI
dev](https://forge.apps.education.fr/lamadone/informatique/premiere-nsi-dev)
dans le quel le développement est effectué. Cela permet de valider
facilement la construction et le rendu du site, de commettre des
informations qui ne sont pas encore publiées pour les élèves, etc.

## Environnement de développement

Pour avoir un environnement de développement similaire, je conseille
d'installer localement sur sa machine (testé sous
[GNU/Linux](http://www.gnu.org/gnu/why-gnu-linux.html) les logiciels
suivants :
+ vscodium avec les extensions / VSCode Web IDE sans extensions:
  + ExcutableBook-Myst
  + TeX
  + Python
+ neovim pour des éditions ponctuelles
+ jupyter
+ jupytext
+ texlive
+ arara
+ Just
+ firefox
