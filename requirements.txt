jupyter-book[sphinx]<2
sphinx-proof
sphinx-exercise
SQLAlchemy~=1.4
jsonschema~=4.17
sphinxext-rediraffe~=0.2
sphinxemoji
sphinxcontrib-mermaid
requests
nb_mypy
ipytest
sphinxcontrib-kroki
scapy
bash_kernel
sphinx-tojupyter
